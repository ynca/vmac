Mixed Model Anova Table (Type 3 tests, LRT-method)

Model: RT ~ Reward * Block + (Reward + Block | SubID)
Data: Trng_DataCompiled_RT
Df full model: 25
        Effect df     Chisq p.value
1       Reward  2 15.13 ***   <.001
2        Block  2  13.32 **    .001
3 Reward:Block  4   10.83 *    .029
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1
Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) [
glmerMod]
 Family: inverse.gaussian  ( 1/mu^2 )
Formula: RT ~ Reward * Block + (Reward + Block | SubID)
   Data: dots$data
Control: ctrl

     AIC      BIC   logLik deviance df.resid 
-41145.8 -40945.0  20597.9 -41195.8    22695 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-5.8333 -0.6073 -0.1387  0.4135  9.2557 

Random effects:
 Groups   Name        Variance Std.Dev. Corr                   
 SubID    (Intercept) 0.09989  0.3161                          
          RewardHR    0.02717  0.1648   -0.15                  
          RewardLR    0.01413  0.1189   -0.21  0.52            
          Block1      0.07206  0.2684   -0.07  0.10  0.04      
          Block2      0.05626  0.2372   -0.11  0.16  0.16  0.32
 Residual             0.06614  0.2572                          
Number of obs: 22720, groups:  SubID, 43

Fixed effects:
                Estimate Std. Error t value Pr(>|z|)    
(Intercept)      4.20245    0.15470  27.164  < 2e-16 ***
RewardHR         0.25112    0.05551   4.524 6.08e-06 ***
RewardLR         0.09096    0.04258   2.136 0.032686 *  
Block1          -0.39637    0.10199  -3.886 0.000102 ***
Block2          -0.22162    0.09055  -2.447 0.014386 *  
RewardHR:Block1  0.16520    0.06473   2.552 0.010712 *  
RewardLR:Block1  0.11288    0.06390   1.766 0.077313 .  
RewardHR:Block2  0.11495    0.05508   2.087 0.036900 *  
RewardLR:Block2  0.06401    0.05419   1.181 0.237511    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Correlation of Fixed Effects:
            (Intr) RwrdHR RwrdLR Block1 Block2 RHR:B1 RLR:B1 RHR:B2
RewardHR    -0.031                                                 
RewardLR    -0.150  0.512                                          
Block1      -0.076  0.121  0.065                                   
Block2      -0.121  0.169  0.155  0.350                            
RwrdHR:Blc1  0.005 -0.021 -0.017 -0.307 -0.014                     
RwrdLR:Blc1  0.005 -0.013 -0.030 -0.311 -0.014  0.491              
RwrdHR:Blc2  0.002 -0.010 -0.007 -0.015 -0.294  0.038  0.024       
RwrdLR:Blc2  0.002 -0.005 -0.016 -0.015 -0.298  0.024  0.042  0.491
Mixed Model Anova Table (Type 3 tests, LRT-method)

Model: RT ~ Reward * Block + (Reward + Block | SubID)
Data: Trng_DataCompiled_RT
Df full model: 25
        Effect df     Chisq p.value
1       Reward  2 15.13 ***   <.001
2        Block  2    4.90 +    .086
3 Reward:Block  4   10.83 *    .029
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1
Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) [
glmerMod]
 Family: inverse.gaussian  ( 1/mu^2 )
Formula: RT ~ Reward * Block + (Reward + Block | SubID)
   Data: dots$data
Control: ctrl

     AIC      BIC   logLik deviance df.resid 
-41145.8 -40945.0  20597.9 -41195.8    22695 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-5.8333 -0.6073 -0.1387  0.4135  9.2557 

Random effects:
 Groups   Name        Variance Std.Dev. Corr                   
 SubID    (Intercept) 0.11157  0.3340                          
          RewardLR    0.02090  0.1446   -0.36                  
          RewardNR    0.02717  0.1648   -0.35  0.71            
          Block1      0.07206  0.2684   -0.02 -0.08 -0.10      
          Block2      0.05626  0.2372   -0.03 -0.05 -0.16  0.32
 Residual             0.06614  0.2572                          
Number of obs: 22720, groups:  SubID, 43

Fixed effects:
                Estimate Std. Error t value Pr(>|z|)    
(Intercept)      4.45357    0.16270  27.374  < 2e-16 ***
RewardLR        -0.16017    0.04973  -3.221  0.00128 ** 
RewardNR        -0.25113    0.05551  -4.524 6.07e-06 ***
Block1          -0.23118    0.10263  -2.253  0.02429 *  
Block2          -0.10666    0.09110  -1.171  0.24169    
RewardLR:Block1 -0.05231    0.06491  -0.806  0.42027    
RewardNR:Block1 -0.16520    0.06473  -2.552  0.01071 *  
RewardLR:Block2 -0.05094    0.05514  -0.924  0.35557    
RewardNR:Block2 -0.11495    0.05508  -2.087  0.03690 *  
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Correlation of Fixed Effects:
            (Intr) RwrdLR RwrdNR Block1 Block2 RLR:B1 RNR:B1 RLR:B2
RewardLR    -0.320                                                 
RewardNR    -0.312  0.678                                          
Block1      -0.032 -0.073 -0.107                                   
Block2      -0.058 -0.052 -0.162  0.343                            
RwrdLR:Blc1  0.003 -0.019 -0.008 -0.324 -0.009                     
RwrdNR:Blc1  0.003 -0.009 -0.021 -0.325 -0.009  0.514              
RwrdLR:Blc2  0.002 -0.013 -0.005 -0.009 -0.312  0.031  0.014       
RwrdNR:Blc2  0.002 -0.005 -0.010 -0.009 -0.312  0.014  0.038  0.517
Mixed Model Anova Table (Type 3 tests, LRT-method)

Model: RT ~ Reward * Block + (Reward + Block | SubID)
Data: Testing_DataCompiled
Df full model: 17
        Effect df     Chisq p.value
1       Reward  2    5.96 +    .051
2        Block  1 11.87 ***   <.001
3 Reward:Block  2      0.79    .675
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1
Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) [
glmerMod]
 Family: inverse.gaussian  ( 1/mu^2 )
Formula: RT ~ Reward * Block + (Reward + Block | SubID)
   Data: dots$data
Control: ctrl

     AIC      BIC   logLik deviance df.resid 
 -9179.8  -9074.0   4606.9  -9213.8     3709 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.8196 -0.6233 -0.1201  0.4375  6.8998 

Random effects:
 Groups   Name        Variance Std.Dev. Corr             
 SubID    (Intercept) 0.27983  0.5290                    
          RewardHR    0.09707  0.3116   -0.24            
          RewardLR    0.06687  0.2586   -0.23  0.73      
          Block2      0.10576  0.3252   -0.49  0.25  0.01
 Residual             0.05501  0.2345                    
Number of obs: 3726, groups:  SubID, 42

Fixed effects:
                Estimate Std. Error t value Pr(>|z|)    
(Intercept)      4.87037    0.19904  24.469  < 2e-16 ***
RewardHR         0.23652    0.10881   2.174 0.029727 *  
RewardLR         0.03269    0.09781   0.334 0.738219    
Block2          -0.40535    0.11054  -3.667 0.000246 ***
RewardHR:Block2  0.05817    0.10314   0.564 0.572759    
RewardLR:Block2 -0.03188    0.10131  -0.315 0.753034    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Correlation of Fixed Effects:
            (Intr) RwrdHR RwrdLR Block2 RHR:B2
RewardHR    -0.136                            
RewardLR    -0.187  0.596                     
Block2      -0.466  0.356  0.257              
RwrdHR:Blc2  0.132 -0.498 -0.270 -0.453       
RwrdLR:Blc2  0.135 -0.247 -0.548 -0.461  0.496
Mixed Model Anova Table (Type 3 tests, LRT-method)

Model: RT ~ Reward * Block + (Reward + Block | SubID)
Data: Testing_DataCompiled
Df full model: 17
        Effect df   Chisq p.value
1       Reward  2  5.96 +    .051
2        Block  1 8.83 **    .003
3 Reward:Block  2    0.79    .675
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1
Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) [
glmerMod]
 Family: inverse.gaussian  ( 1/mu^2 )
Formula: RT ~ Reward * Block + (Reward + Block | SubID)
   Data: dots$data
Control: ctrl

     AIC      BIC   logLik deviance df.resid 
 -9179.8  -9074.0   4606.9  -9213.8     3709 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.8196 -0.6233 -0.1201  0.4375  6.8998 

Random effects:
 Groups   Name        Variance Std.Dev. Corr             
 SubID    (Intercept) 0.29617  0.5442                    
          RewardLR    0.04587  0.2142   -0.25            
          RewardNR    0.09707  0.3116   -0.33  0.57      
          Block2      0.10576  0.3252   -0.33 -0.35 -0.25
 Residual             0.05501  0.2345                    
Number of obs: 3726, groups:  SubID, 42

Fixed effects:
                Estimate Std. Error t value Pr(>|z|)    
(Intercept)      5.10689    0.21343  23.928  < 2e-16 ***
RewardLR        -0.20383    0.09339  -2.183  0.02907 *  
RewardNR        -0.23652    0.10881  -2.174  0.02973 *  
Block2          -0.34718    0.11196  -3.101  0.00193 ** 
RewardLR:Block2 -0.09005    0.10261  -0.878  0.38018    
RewardNR:Block2 -0.05817    0.10315  -0.564  0.57276    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Correlation of Fixed Effects:
            (Intr) RwrdLR RwrdNR Block2 RLR:B2
RewardLR    -0.311                            
RewardNR    -0.383  0.541                     
Block2      -0.370  0.130  0.107              
RwrdLR:Blc2  0.131 -0.582 -0.257 -0.474       
RwrdNR:Blc2  0.131 -0.297 -0.498 -0.474  0.515
 Reward emmean    SE  df asymp.LCL asymp.UCL
 HR       4.20 0.155 Inf      3.90      4.51
 LR       4.45 0.163 Inf      4.13      4.77
 NR       4.29 0.154 Inf      3.99      4.60

Results are averaged over the levels of: Block 
Results are given on the 1/mu^2 (not the response) scale. 
Confidence level used: 0.95 
 Block emmean    SE  df asymp.LCL asymp.UCL
 1       4.52 0.172 Inf      4.18      4.86
 2       4.22 0.161 Inf      3.90      4.53
 3       4.21 0.160 Inf      3.90      4.52

Results are averaged over the levels of: Reward 
Results are given on the 1/mu^2 (not the response) scale. 
Confidence level used: 0.95 
Block = 1:
 Reward emmean    SE  df asymp.LCL asymp.UCL
 HR       4.47 0.176 Inf      4.13      4.82
 LR       4.60 0.180 Inf      4.25      4.96
 NR       4.49 0.173 Inf      4.15      4.83

Block = 2:
 Reward emmean    SE  df asymp.LCL asymp.UCL
 HR       4.08 0.162 Inf      3.76      4.40
 LR       4.37 0.170 Inf      4.04      4.71
 NR       4.20 0.161 Inf      3.89      4.52

Block = 3:
 Reward emmean    SE  df asymp.LCL asymp.UCL
 HR       4.05 0.159 Inf      3.74      4.37
 LR       4.38 0.170 Inf      4.05      4.72
 NR       4.19 0.161 Inf      3.87      4.50

Results are given on the 1/mu^2 (not the response) scale. 
Confidence level used: 0.95 
 Reward emmean    SE  df asymp.LCL asymp.UCL
 HR       4.67 0.180 Inf      4.31      5.02
 LR       4.93 0.200 Inf      4.54      5.32
 NR       4.68 0.186 Inf      4.32      5.05

Results are averaged over the levels of: Block 
Results are given on the 1/mu^2 (not the response) scale. 
Confidence level used: 0.95 
 Block emmean    SE  df asymp.LCL asymp.UCL
 1       4.96 0.197 Inf      4.57      5.35
 2       4.56 0.179 Inf      4.21      4.91

Results are averaged over the levels of: Reward 
Results are given on the 1/mu^2 (not the response) scale. 
Confidence level used: 0.95 
Block = 1:
 Reward emmean    SE  df asymp.LCL asymp.UCL
 HR       4.87 0.199 Inf      4.48      5.26
 LR       5.11 0.213 Inf      4.69      5.53
 NR       4.90 0.205 Inf      4.50      5.30

Block = 2:
 Reward emmean    SE  df asymp.LCL asymp.UCL
 HR       4.47 0.177 Inf      4.12      4.81
 LR       4.76 0.201 Inf      4.37      5.15
 NR       4.47 0.184 Inf      4.11      4.83

Results are given on the 1/mu^2 (not the response) scale. 
Confidence level used: 0.95 
Mixed Model Anova Table (Type 3 tests, LRT-method)

Model: RT ~ Reward * Block + (Reward:Block | SubID)
Data: Testing_DataCompiled
Df full model: 35
        Effect df   Chisq p.value
1       Reward  2  6.01 *    .049
2        Block  1 6.96 **    .008
3 Reward:Block  2    0.10    .952
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1
Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) [
glmerMod]
 Family: inverse.gaussian  ( 1/mu^2 )
Formula: RT ~ Reward * Block + (Reward:Block | SubID)
   Data: dots$data
Control: ctrl

     AIC      BIC   logLik deviance df.resid 
 -9163.8  -8946.0   4616.9  -9233.8     3691 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.8028 -0.6224 -0.1238  0.4345  6.9490 

Random effects:
 Groups   Name            Variance Std.Dev. Corr                               
 SubID    (Intercept)     0.12515  0.3538                                      
          RewardNR:Block1 0.10994  0.3316    0.16                              
          RewardHR:Block1 0.07274  0.2697    0.56  0.34                        
          RewardLR:Block1 0.12104  0.3479    0.21  0.61  0.65                  
          RewardNR:Block2 0.10104  0.3179    0.13  0.08  0.14  0.09            
          RewardHR:Block2 0.10022  0.3166    0.23 -0.04  0.33 -0.04  0.08      
          RewardLR:Block2 0.09157  0.3026    0.20 -0.04  0.35 -0.07  0.17  0.40
 Residual                 0.05438  0.2332                                      
Number of obs: 3726, groups:  SubID, 42

Fixed effects:
                Estimate Std. Error t value Pr(>|z|)    
(Intercept)      4.86731    0.19505  24.954  < 2e-16 ***
RewardHR         0.27713    0.11920   2.325  0.02008 *  
RewardLR         0.04608    0.10694   0.431  0.66653    
Block2          -0.37229    0.13194  -2.822  0.00478 ** 
RewardHR:Block2 -0.01407    0.16319  -0.086  0.93128    
RewardLR:Block2 -0.04891    0.16273  -0.301  0.76376    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Correlation of Fixed Effects:
            (Intr) RwrdHR RwrdLR Block2 RHR:B2
RewardHR    -0.088                            
RewardLR    -0.172  0.554                     
Block2      -0.412  0.496  0.372              
RwrdHR:Blc2  0.093 -0.623 -0.404 -0.694       
RwrdLR:Blc2  0.125 -0.259 -0.668 -0.553  0.548
convergence code: 0
unable to evaluate scaled gradient
Model failed to converge: degenerate  Hessian with 1 negative eigenvalues

Mixed Model Anova Table (Type 3 tests, LRT-method)

Model: RT ~ Reward * Block + (Reward:Block | SubID)
Data: Testing_DataCompiled
Df full model: 35
        Effect df   Chisq p.value
1       Reward  2  6.01 *    .049
2        Block  1 8.96 **    .003
3 Reward:Block  2    0.10    .952
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘+’ 0.1 ‘ ’ 1
Generalized linear mixed model fit by maximum likelihood (Laplace Approximation) [
glmerMod]
 Family: inverse.gaussian  ( 1/mu^2 )
Formula: RT ~ Reward * Block + (Reward:Block | SubID)
   Data: dots$data
Control: ctrl

     AIC      BIC   logLik deviance df.resid 
 -9163.8  -8946.0   4616.9  -9233.8     3691 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-2.8028 -0.6224 -0.1238  0.4345  6.9490 

Random effects:
 Groups   Name            Variance Std.Dev. Corr                               
 SubID    (Intercept)     0.11758  0.3429                                      
          RewardHR:Block1 0.07309  0.2703    0.62                              
          RewardLR:Block1 0.12283  0.3505    0.24  0.65                        
          RewardNR:Block1 0.10566  0.3250    0.22  0.33  0.60                  
          RewardHR:Block2 0.09930  0.3151    0.28  0.33 -0.04 -0.07            
          RewardLR:Block2 0.09318  0.3053    0.24  0.36 -0.06 -0.06  0.40      
          RewardNR:Block2 0.09507  0.3083    0.20  0.11  0.07  0.03  0.04  0.15
 Residual                 0.05438  0.2332                                      
Number of obs: 3726, groups:  SubID, 42

Fixed effects:
                Estimate Std. Error t value Pr(>|z|)    
(Intercept)      5.14444    0.21950  23.437  < 2e-16 ***
RewardLR        -0.23105    0.10728  -2.154  0.03126 *  
RewardNR        -0.27713    0.11920  -2.325  0.02008 *  
Block2          -0.38637    0.11895  -3.248  0.00116 ** 
RewardLR:Block2 -0.03484    0.15493  -0.225  0.82209    
RewardNR:Block2  0.01407    0.16319   0.086  0.93128    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Correlation of Fixed Effects:
            (Intr) RwrdLR RwrdNR Block2 RLR:B2
RewardLR    -0.369                            
RewardNR    -0.465  0.558                     
Block2      -0.458  0.197  0.304              
RwrdLR:Blc2  0.238 -0.702 -0.384 -0.488       
RwrdNR:Blc2  0.256 -0.290 -0.623 -0.602  0.478
convergence code: 0
unable to evaluate scaled gradient
Model failed to converge: degenerate  Hessian with 4 negative eigenvalues

Block = 1:
 contrast estimate     SE  df z.ratio p.value
 NR - HR   -0.1302 0.0679 Inf  -1.918  0.1335
 NR - LR   -0.0132 0.0576 Inf  -0.229  0.9715
 HR - LR    0.1170 0.0631 Inf   1.854  0.1524

Block = 2:
 contrast estimate     SE  df z.ratio p.value
 NR - HR   -0.2954 0.0661 Inf  -4.471  <.0001
 NR - LR   -0.1261 0.0553 Inf  -2.280  0.0586
 HR - LR    0.1693 0.0616 Inf   2.751  0.0164

Block = 3:
 contrast estimate     SE  df z.ratio p.value
 NR - HR   -0.3278 0.0663 Inf  -4.947  <.0001
 NR - LR   -0.1336 0.0554 Inf  -2.412  0.0419
 HR - LR    0.1941 0.0615 Inf   3.158  0.0045

Note: contrasts are still on the 1/mu^2 scale 
P value adjustment: tukey method for comparing a family of 3 estimates 
