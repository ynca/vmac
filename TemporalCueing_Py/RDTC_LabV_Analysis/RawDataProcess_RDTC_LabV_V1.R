## RawDataProcess_RDTC_LabV_V1.R
## This script is for preprocessing raw labvanced data files for VITA RDTC
## Version 1, 1 Jan 2023

# Set working directory
rm(list=ls())

# Import filenames
FilenameList <- readxl::read_xlsx('SubIDList.xlsx', sheet = "V2_V3_V4")


##### START OF SECTION FOR PREPROCESSING TRAINING PHASE DATA #####
# Create data frome to store compiled data
Trng_DataCompiled <- data.frame(matrix(NA,    # Create empty data frame
                                       nrow = 0,
                                       ncol = 16))
colnames(Trng_DataCompiled) <- c("SubID", "TrialNum", "Targ", "TargPos", "Group", 
                                 "Reward", "RESP", "ACC", 
                                 "RT_FromFramePress", "RESP_Frame",
                                 "Block", "BlkTrialNum", "RewTrOrder",
                                 "RESP_Frame_Num", "RT_Calc", "Distractor")

#Create matrix to identify & store which pts needs to be excluded based on accuracy
Trng_ToExcludeList <- matrix(data = NA, nrow = dim(FilenameList)[1], ncol = 8)
colnames(Trng_ToExcludeList) <- c("SubID", "ToExclude", "Num_NATrials", "Num_SlowTrials", 
                                  "Num_TotalExcludedTrials", "Group",
                                  "Age", "Sex")

#Create matrix to store practice accuracy score
Trng_PracScore <- matrix(data = NA, nrow = dim(FilenameList)[1], ncol = 3)
colnames(Trng_PracScore) <- c("SubID", "PracScore", "Group")

for(row in 1:dim(FilenameList)[1]) {
  #Extract the participant's raw data 
  SubRawData <- read.csv(paste('trials_', FilenameList$LabVSession[row], 
                               '.csv', sep = ""))
  ## Check practice accuracy score
  #SubPracAccScore <- tail(SubRawData$ACC_Total_Prac[SubRawData$Block_Name == 
  #                                                    "block_Practice"], n = 1)
  SubPracAccScore <- max(SubRawData$ACC_Total_Prac[SubRawData$Task_Name == 
                                                     "task_Trng_Prac_Fast"], 
                         na.rm = TRUE)
  #Store practice accuracy score to the compiled matrix
  Trng_PracScore[row,1] <- SubRawData$Rec_Session_Id[1]
  Trng_PracScore[row,2] <- SubPracAccScore
  Trng_PracScore[row,3] <- FilenameList$Group[row]
  
  #Segment raw data for Training phase
  SubRawData_Trng <- SubRawData[SubRawData$Task_Name == "task_Trng",]
  
  
  # Select columns
  SubRawData_Sel <- data.frame(matrix(NA,    # Create empty data frame
                                      nrow = 360,
                                      ncol = 14))
  colnames(SubRawData_Sel) <- c("SubID", "TrialNum", "Targ", "TargPos", "Group", 
                                "Reward", "RESP", "ACC", 
                                "RT_FromFramePress", "RESP_Frame",
                                "Block", "BlkTrialNum", "RewTrOrder", "Distractor")
  
  SubRawData_Sel$SubID <- SubRawData_Trng$Rec_Session_Id
  SubRawData_Sel$TrialNum <- SubRawData_Trng$Trial_Nr
  SubRawData_Sel$Targ <- SubRawData_Trng$Targ
  SubRawData_Sel$TargPos <- SubRawData_Trng$TargPos
  SubRawData_Sel$Group <- FilenameList$Group[row] #to change to the pt's assigned group
  SubRawData_Sel$Reward <- SubRawData_Trng$Reward
  SubRawData_Sel$RESP <- SubRawData_Trng$RESP
  SubRawData_Sel$ACC <- SubRawData_Trng$ACC
  SubRawData_Sel$RT_FromFramePress <- SubRawData_Trng$RT_FromFramePress
  SubRawData_Sel$RESP_Frame <- SubRawData_Trng$RESP_Frame
  SubRawData_Sel$Block <- SubRawData_Trng$Condition_Id
  SubRawData_Sel$BlkTrialNum <- SubRawData_Trng$BlkTrial_Nr
  SubRawData_Sel$Distractor <- SubRawData_Trng$DistractType
  
  # Sort by reward then by Overall_TrOrder
  SubRawData_Sel <- SubRawData_Sel[order(SubRawData_Sel$Reward, 
                                         SubRawData_Sel$TrialNum),]
  # Add in TrOrder for each reward condition
  #There are 120 trials for each reward condition
  SubRawData_Sel$RewTrOrder <- rep.int(1:120, times = 3)
  
  # Sort data frame back to by overall trial order
  SubRawData_Sel <- SubRawData_Sel[order(SubRawData_Sel$TrialNum),]
  
  # Add SubID to exclusion df
  Trng_ToExcludeList[row,1] <- SubRawData_Sel$SubID[1]
  # Add Group to exclusion df
  Trng_ToExcludeList[row,6] <- FilenameList$Group[row]
  # Add Age
  Trng_ToExcludeList[row,7] <- SubRawData$Age[!is.na(SubRawData$Age)]
  # Add Sex
  Trng_ToExcludeList[row,8] <- max(SubRawData$Sex)
  
  #Check if pt is wrong for >75% of trials (i.e. correct >270 trials)
  SubNumCorrect <- sum(SubRawData_Sel$ACC)
  if (SubNumCorrect > 270) {
    ToExclude <- 'No'
  } else {
    ToExclude <- 'Yes'
  } 
  Trng_ToExcludeList[row,2] <- ToExclude
  
  # Trim missing responses
  SubRawData_Sel_Trim <- SubRawData_Sel[SubRawData_Sel$RESP != "",]
  Trng_ToExcludeList[row,3] <- nrow(SubRawData_Sel) - nrow(SubRawData_Sel_Trim) #Count the no. of missing trials for pt
  
  ## Extract number from RT_fromFramePress 
  SubRawData_Sel_Trim$RESP_Frame_Num <- gsub("[^0-9.-]", 
                                             "", 
                                             SubRawData_Sel_Trim$RESP_Frame)
  ## Calculate RT 
  SubRawData_Sel_Trim$RESP_Frame_Num <- 
    as.integer(SubRawData_Sel_Trim$RESP_Frame_Num)
  
  #Calculate and store actual RT to new column
  SubRawData_Sel_Trim$RT_calc <- 
    ((SubRawData_Sel_Trim$RESP_Frame_Num - SubRawData_Sel_Trim$TargPos) * 117 + 
       SubRawData_Sel_Trim$RT_FromFramePress)
  
  #Duration: PreISI1 = 532ms, PreISI2 = 1468ms, ISI = 1000ms
  ## Calculate and assign RT for responses that fall on PreISI1
  for(j in 1:dim(SubRawData_Sel_Trim)[1]){
    if (SubRawData_Sel_Trim$RESP_Frame[j] == "PreISI1"){
      SubRawData_Sel_Trim$RT_calc[j] <- 
        (17 - SubRawData_Sel_Trim$TargPos[j]) * 117 + 
        SubRawData_Sel_Trim$RT_FromFramePress[j]
    } else {SubRawData_Sel_Trim$RT_calc[j] <- SubRawData_Sel_Trim$RT_calc[j]}
  }
  ## Calculate and assign RT for responses that fall on PreISI2
  for(j in 1:dim(SubRawData_Sel_Trim)[1]){
    if (SubRawData_Sel_Trim$RESP_Frame[j] == "PreISI2"){
      SubRawData_Sel_Trim$RT_calc[j] <- 
        (17 - SubRawData_Sel_Trim$TargPos[j]) * 117 + 532 + 
        SubRawData_Sel_Trim$RT_FromFramePress[j]
    } else {SubRawData_Sel_Trim$RT_calc[j] <- SubRawData_Sel_Trim$RT_calc[j]}
  }  
  ## Calculate and assign RT for responses that fall on ISI
  for(j in 1:dim(SubRawData_Sel_Trim)[1]){
    if (SubRawData_Sel_Trim$RESP_Frame[j] == "ISI"){
      SubRawData_Sel_Trim$RT_calc[j] <- 
        (17 - SubRawData_Sel_Trim$TargPos[j]) * 117 + 532 + 1468 +
        SubRawData_Sel_Trim$RT_FromFramePress[j]
    } else {SubRawData_Sel_Trim$RT_calc[j] <- SubRawData_Sel_Trim$RT_calc[j]}
  } 
  
  #Trim slow responses (i.e. RT > 1000ms)
  SubRawData_Sel_Trim <- subset(SubRawData_Sel_Trim, RT_calc < 1000)
  Trng_ToExcludeList[row,4] <- nrow(SubRawData_Sel) - nrow(SubRawData_Sel_Trim) - 
    as.numeric(Trng_ToExcludeList[row,3]) #Count the no. of slow trials for pt
  Trng_ToExcludeList[row,5] <- 
    sum(as.numeric(Trng_ToExcludeList[row,3]), 
        as.numeric(Trng_ToExcludeList[row,4]))
  
  # Concetenate data to compiled df
  Trng_DataCompiled <- rbind(Trng_DataCompiled, SubRawData_Sel_Trim)
}

## Write CSVs
write.csv(Trng_PracScore, file = "RDTC_Trng_PracScore_V1.csv")
write.csv(Trng_ToExcludeList, file = "RDTC_Trng_ToExcludeList_V1.csv")
write.csv(Trng_DataCompiled, file = "RDTC_Trng_DataCompiled_V1.csv")
##### END OF SECTION FOR PREPROCESSING TRAINING PHASE DATA #####


##### START OF SECTION FOR PREPROCESSING TESTING PHASE DATA #####
rm(list=ls())
# Import filenames
FilenameList <- readxl::read_xlsx('SubIDList.xlsx', sheet = "V2_V3_V4")
# Create data frome to store compiled data
Testing_DataCompiled <- data.frame(matrix(NA,    # Create empty data frame
                                          nrow = 0,
                                          ncol = 16))
colnames(Testing_DataCompiled) <- c("SubID", "TrialNum", "Targ", "TargPos", "Group", 
                                    "Reward", "RESP", "ACC", 
                                    "RT_FromFramePress", "RESP_Frame",
                                    "Block", "BlkTrialNum", "RewTrOrder",
                                    "RESP_Frame_Num", "RT_Calc", "Distractor")

#Create matrix to identify & store which pts needs to be excluded based on accuracy
Testing_ToExcludeList <- matrix(data = NA, nrow = dim(FilenameList)[1], 
                                ncol = 6)
colnames(Testing_ToExcludeList) <- c("SubID", "ToExclude", "Num_NATrials", "Num_SlowTrials", 
                                     "Num_TotalExcludedTrials", "Group")

for(row in 1:dim(FilenameList)[1]) {
  #Extract the participant's raw data 
  SubRawData <- read.csv(paste('trials_', FilenameList$LabVSession[row], 
                               '.csv', sep = ""))
  #Segment raw data for Testing phase
  SubRawData_Testing <- SubRawData[SubRawData$Task_Name == "task_Testing",]
  
  
  # Select columns
  SubRawData_Sel <- data.frame(matrix(NA,    # Create empty data frame
                                      nrow = 180,
                                      ncol = 14))
  colnames(SubRawData_Sel) <- c("SubID", "TrialNum", "Targ", "TargPos", "Group", 
                                "Reward", "RESP", "ACC", 
                                "RT_FromFramePress", "RESP_Frame",
                                "Block", "BlkTrialNum", "RewTrOrder", "Distractor")
  SubRawData_Sel$SubID <- SubRawData_Testing$Rec_Session_Id
  SubRawData_Sel$TrialNum <- SubRawData_Testing$Trial_Nr
  SubRawData_Sel$Targ <- SubRawData_Testing$Targ
  SubRawData_Sel$TargPos <- SubRawData_Testing$TargPos
  SubRawData_Sel$Group <- FilenameList$Group[row]
  SubRawData_Sel$Reward <- SubRawData_Testing$Reward
  SubRawData_Sel$RESP <- SubRawData_Testing$RESP
  SubRawData_Sel$ACC <- SubRawData_Testing$ACC
  SubRawData_Sel$RT_FromFramePress <- SubRawData_Testing$RT_FromFramePress
  SubRawData_Sel$RESP_Frame <- SubRawData_Testing$RESP_Frame
  SubRawData_Sel$Block <- SubRawData_Testing$Condition_Id
  SubRawData_Sel$BlkTrialNum <- SubRawData_Testing$BlkTrial_Nr
  SubRawData_Sel$Distractor <- SubRawData_Testing$DistractType
  
  # Sort by reward then by Overall_TrOrder
  SubRawData_Sel <- SubRawData_Sel[order(SubRawData_Sel$Reward, 
                                         SubRawData_Sel$TrialNum),]
  # Add in TrOrder for each reward condition
  #There are 60 trials for each reward condition
  SubRawData_Sel$RewTrOrder <- rep.int(1:60, times = 3)
  
  # Sort data frame back to by overall trial order
  SubRawData_Sel <- SubRawData_Sel[order(SubRawData_Sel$TrialNum),]
  
  
  # Add SubID to exclusion df
  Testing_ToExcludeList[row,1] <- SubRawData_Sel$SubID[1]
  # Add Group to exclusion df
  Testing_ToExcludeList[row,6] <- FilenameList$Group[row]
  
  #Check if pt is wrong for >75% of trials (i.e. correct >135 trials)
  SubNumCorrect <- sum(SubRawData_Sel$ACC)
  if (SubNumCorrect > 135) {
    ToExclude <- 'No'
  } else {
    ToExclude <- 'Yes'
  } 
  Testing_ToExcludeList[row,2] <- ToExclude
  
  # Trim missing responses
  SubRawData_Sel_Trim <- SubRawData_Sel[SubRawData_Sel$RESP != "",]
  Testing_ToExcludeList[row,3] <- nrow(SubRawData_Sel) - nrow(SubRawData_Sel_Trim) #Count the no. of missing trials for pt
  
  ## Extract number from RT_fromFramePress 
  SubRawData_Sel_Trim$RESP_Frame_Num <- gsub("[^0-9.-]", 
                                             "", 
                                             SubRawData_Sel_Trim$RESP_Frame)
  #Set Resp_Frame_Num as int
  SubRawData_Sel_Trim$RESP_Frame_Num <- as.integer(SubRawData_Sel_Trim$RESP_Frame_Num)
  
  #Calculate and store actual RT to new column
  SubRawData_Sel_Trim$RT_calc <- 
    ((SubRawData_Sel_Trim$RESP_Frame_Num - SubRawData_Sel_Trim$TargPos) * 117 + 
       SubRawData_Sel_Trim$RT_FromFramePress)
  
  #Duration: PreISI1 = 532ms, PreISI2 = 1468ms, ISI = 1000ms
  ## Calculate and assign RT for responses that fall on PreISI1
  for(j in 1:dim(SubRawData_Sel_Trim)[1]){
    if (SubRawData_Sel_Trim$RESP_Frame[j] == "PreISI1"){
      SubRawData_Sel_Trim$RT_calc[j] <- 
        (17 - SubRawData_Sel_Trim$TargPos[j]) * 117 + 
        SubRawData_Sel_Trim$RT_FromFramePress[j]
    } else {SubRawData_Sel_Trim$RT_calc[j] <- SubRawData_Sel_Trim$RT_calc[j]}
  }
  ## Calculate and assign RT for responses that fall on PreISI2
  for(j in 1:dim(SubRawData_Sel_Trim)[1]){
    if (SubRawData_Sel_Trim$RESP_Frame[j] == "PreISI2"){
      SubRawData_Sel_Trim$RT_calc[j] <- 
        (17 - SubRawData_Sel_Trim$TargPos[j]) * 117 + 532 + 
        SubRawData_Sel_Trim$RT_FromFramePress[j]
    } else {SubRawData_Sel_Trim$RT_calc[j] <- SubRawData_Sel_Trim$RT_calc[j]}
  }  
  ## Calculate and assign RT for responses that fall on ISI
  for(j in 1:dim(SubRawData_Sel_Trim)[1]){
    if (SubRawData_Sel_Trim$RESP_Frame[j] == "ISI"){
      SubRawData_Sel_Trim$RT_calc[j] <- 
        (17 - SubRawData_Sel_Trim$TargPos[j]) * 117 + 532 + 1468 +
        SubRawData_Sel_Trim$RT_FromFramePress[j]
    } else {SubRawData_Sel_Trim$RT_calc[j] <- SubRawData_Sel_Trim$RT_calc[j]}
  } 
  
  #Trim slow responses (i.e. RT > 1000ms)
  SubRawData_Sel_Trim <- subset(SubRawData_Sel_Trim, RT_calc < 1000)
  Testing_ToExcludeList[row,4] <- nrow(SubRawData_Sel) - nrow(SubRawData_Sel_Trim) - 
    as.numeric(Testing_ToExcludeList[row,3]) #Count the no. of slow trials for pt
  Testing_ToExcludeList[row,5] <- 
    sum(as.numeric(Testing_ToExcludeList[row,3]), 
        as.numeric(Testing_ToExcludeList[row,4]))
  
  # Concetenate data to compiled df
  Testing_DataCompiled <- rbind(Testing_DataCompiled, SubRawData_Sel_Trim)
}

## Write CSVs
write.csv(Testing_ToExcludeList, file = "RDTC_Testing_ToExcludeList_V1.csv")
write.csv(Testing_DataCompiled, file = "RDTC_Testing_DataCompiled_V1.csv")
