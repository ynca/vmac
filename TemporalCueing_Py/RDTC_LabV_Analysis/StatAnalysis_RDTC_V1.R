## StatAnalysis_RDTC_V1.R
## This is a script for statistical analyses of preprocessed data for VITA+1 
## Also termed RDTC for labvanced sake
## Version 1, 1 Feb 2023

rm(list=ls())

######### Start of Analysis of Training task ######################
############### Start of Examine ACC rate and slowness trends#############
## Read compiled RT data
Trng_DataCompiled <- read.csv('RDTC_Trng_DataCompiled_V1.csv')
Trng_ToExcludeList <- read.csv("RDTC_Trng_ToExcludeList_V1.csv")

## Organise the dataset for examining performance for excluded vs not excluded
# SubID, ToExclude, OverallACCRate, Block, AccRate

# Create data frame to compile data
PerfComp_Compiled <- data.frame(matrix(NA,    # Create empty data frame
                                       nrow = 0,
                                       ncol = 6))
colnames(PerfComp_Compiled) <- c("SubID", "ToExclude", "OverallAccRate", 
                                 "Block", "AccRate", "NumSlowMissingTrials")


for(sub in 1:dim(Trng_ToExcludeList)[1]){
  # Create empty temporary data frame to store the sub's extracted data
  # There are 10 blocks for training phase
  PerfComp_Sub <- data.frame(matrix(NA,    # Create empty data frame
                                    nrow = 10,
                                    ncol = 6))
  colnames(PerfComp_Sub) <- c("SubID", "ToExclude", "OverallAccRate", 
                              "Block", "AccRate", "NumSlowMissingTrials")
  
  # Extract pt's raw data
  SubData <- Trng_DataCompiled[Trng_DataCompiled$SubID == 
                                 Trng_ToExcludeList$SubID[sub],]
  
  PerfComp_Sub$SubID <- Trng_ToExcludeList$SubID[sub]
  PerfComp_Sub$ToExclude <- Trng_ToExcludeList$ToExclude[sub]
  
  # Calculate overall accuracy rate
  PerfComp_Sub$OverallAccRate <- mean(SubData$ACC)
  
  # Calculate mean accuracy for each block
  # For training phase, there are 10 blocks
  # Each block, there are 36 trials
  for (bl in 1:10){
    PerfComp_Sub$Block[bl] <- bl
    PerfComp_Sub$AccRate[bl] <- mean(SubData$ACC[SubData$Block == bl])
    PerfComp_Sub$NumSlowMissingTrials[bl] <- 
      36 - sum(SubData$ACC[SubData$Block == bl])
  }
  
  # compile into PerfComp_Compiled df
  PerfComp_Compiled <- rbind(PerfComp_Compiled, PerfComp_Sub)
}

library(ggplot2)

ggplot(data = PerfComp_Compiled, 
       aes(x = Block, y = AccRate, colour = ToExclude)) +
  geom_point() +
  geom_smooth(method = "loess")

ggplot(data = PerfComp_Compiled, 
       aes(x = Block, y = NumSlowMissingTrials, colour = ToExclude)) +
  geom_point() +
  geom_smooth(method = "loess")

## Write csv for PerfComp_Compiled
write.csv(PerfComp_Compiled, "PerfComp_Compiled.csv")


############## Start of formal data analysis ###############
rm(list=ls())

## Read compiled RT data
Trng_DataCompiled <- read.csv('RDTC_Trng_DataCompiled_V1.csv')
Trng_ToExcludeList <- read.csv("RDTC_Trng_ToExcludeList_V1.csv")

## Create new data frame to select pt's data
Trng_DataCompiled_Selected <- data.frame(matrix(NA,    # Create empty data frame
                                                nrow = 0,
                                                ncol = 17))
colnames(Trng_DataCompiled_Selected) <- c("X","SubID", "TrialNum", "Targ", "TargPos", "Group", 
                                          "Reward", "RESP", "ACC", 
                                          "RT_FromFramePress", "RESP_Frame",
                                          "Block", "BlkTrialNum", "RewTrOrder", "Distractor",
                                          "RESP_Frame_Num", "RT_Calc")

## Subset Trng_ToExcludeList_V1 to select ToExclude == No
Trng_ToExcludeList_V1_Selected <- subset(Trng_ToExcludeList, 
                                         ToExclude == "No")
for(sub in 1:dim(Trng_ToExcludeList_V1_Selected)[1]){
  DataSelect <- Trng_DataCompiled[
    Trng_DataCompiled$SubID == Trng_ToExcludeList_V1_Selected$SubID[sub],]
  Trng_DataCompiled_Selected <- rbind(Trng_DataCompiled_Selected, DataSelect)
}

## Excluded participants are 647311 651845 654825 662953

## Examine sample characteristics
# Count total number of pts included in analysis
length(unique(Trng_DataCompiled_Selected$SubID))

#Count number of pts in each counterbalance group
length(unique(Trng_DataCompiled_Selected$SubID[
  Trng_DataCompiled_Selected$Group == 1]))
length(unique(Trng_DataCompiled_Selected$SubID[
  Trng_DataCompiled_Selected$Group == 2]))
length(unique(Trng_DataCompiled_Selected$SubID[
  Trng_DataCompiled_Selected$Group == 3]))

## Plot histogram to examine distribution
library(ggplot2)

Trng_RT_histo <- ggplot(Trng_DataCompiled_Selected, aes(x = RT_calc)) 
Trng_RT_histo + geom_histogram()

## Fit RT by Reward Trial Number based based on reward condition
ggplot(data = Trng_DataCompiled_Selected, 
       aes(RewTrOrder, RT_calc, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "loess")

ggplot(data = Trng_DataCompiled_Selected, 
       aes(RewTrOrder, RT_calc, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "lm")

## Fit ACC by Reward Trial Number based based on reward condition
ggplot(data = Trng_DataCompiled_Selected, 
       aes(RewTrOrder, ACC, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "loess")

ggplot(data = Trng_DataCompiled_Selected, 
       aes(RewTrOrder, ACC, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "lm")

Trng_DataCompiled_Selected$SubID <- as.factor(Trng_DataCompiled_Selected$SubID)
Trng_DataCompiled_Selected$Distractor <- as.factor(Trng_DataCompiled_Selected$Distractor)
Trng_DataCompiled_Selected$Reward <- as.factor(Trng_DataCompiled_Selected$Reward)

## Make RewTrOrder scale from 0 to 1 
## Since RewTrOrder are 1: 120 we'll divide everything by 120
Trng_DataCompiled_Selected$cRewTrOrder <- 
  Trng_DataCompiled_Selected$RewTrOrder / 120

## Rescale RT_calc from ms to s
## Divide by 1000
Trng_DataCompiled_Selected$cRT_calc <- Trng_DataCompiled_Selected$RT_calc / 1000

library(lme4)
library(afex)
library(car)

# Fit RT for correct responses only
Trng_GLMM_1 <- glmer(RT_calc ~ Reward * cRewTrOrder * Distractor +
                       (1 + Reward + cRewTrOrder + Distractor | SubID),
                     data = Trng_DataCompiled_Selected, 
                     subset = (ACC == 1),
                     family = Gamma(link = "log")) # fitted but with convergence warning

CheckFit_1 <- allFit(Trng_GLMM_1)

CheckFit_1.ok <- CheckFit_1[sapply(CheckFit_1, is, "merMod")]
lapply(CheckFit_1.ok, function(x)   
       x@optinfo$conv$lme4$messages) # only bobyqa fitted with no warnings.

# Proceed with best fitting optimiser
Trng_GLMM_1A <- update(Trng_GLMM_1, 
                       control=glmerControl(optimizer="optimx", 
                                           optCtrl=list(maxfun=2e5, 
                                                        method="bobyqa")))
# Trng_GLMM_1A fitted with warning on different scalings which is minor.

# As Trng_GLMM_1A took a lot of time to process, load the previous saved file to continue
Trng_GLMM_1A <- readRDS("Trng_GLMM_1A.rds")

anova(Trng_GLMM_1A)
Anova(Trng_GLMM_1A)

# Probe Reward:cRewTrOrder & cRewTrOrder:Distractor sig interactions
library(emmeans)

# Reward:cRewTrOrder
Trng_GLMM_1A.slopes.1 <- emtrends(Trng_GLMM_1A, "Reward", "cRewTrOrder", 
                                  type = "response")
Trng_GLMM_1A.slopes.1
pairs(Trng_GLMM_1A.slopes.1)

# Store the plot information to plot using ggplot2
Trng_GLMM_1A.slopes.1.PlotInfo <- emmip(Trng_GLMM_1A, Reward ~ cRewTrOrder, 
                                      at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)),
                                      type = "response",
                                      CIs = TRUE,
                                      plotit = FALSE)
Trng_GLMM_1A.slopes.1.PlotInfo.means <- emmip(Trng_GLMM_1A, Reward ~ cRewTrOrder, 
                                        at = list(cRewTrOrder = c(0.125,.375,.625,.875)),
                                        type = "response",
                                        CIs = TRUE,
                                        plotit = FALSE)


library(ggplot2)

Plot.Trng_GLMM_1A.slopes.1 <- 
  ggplot(data = Trng_GLMM_1A.slopes.1.PlotInfo, 
         aes(x = cRewTrOrder, y = t_yvar, color = Reward)) +
  geom_line() + 
  geom_ribbon(aes(ymax = t_yvar + t_SE, ymin = t_yvar - t_SE), alpha = 0.2,
              linetype = 0) +
  geom_jitter(data = Trng_ScatterTable,
              mapping = aes(x = cRewTrOrder, y = RT_calc, color = Reward),
              alpha = 0.5, shape = 17) +
  xlab("Proportion of trials") + ylab("RT")
Plot.Trng_GLMM_1A.slopes.1 

# For CoRN2023
Plot.Trng_GLMM_1A.slopes.1 <- 
  ggplot(data = Trng_GLMM_1A.slopes.1.PlotInfo, 
         aes(x = cRewTrOrder, y = t_yvar, color = Reward)) +
  geom_line() + 
  geom_ribbon(aes(ymax = t_yvar + t_SE, ymin = t_yvar - t_SE), alpha = 0.2,
              linetype = 0) + 
  xlab("Proportion of trials") + ylab("RT (ms)")
Plot.Trng_GLMM_1A.slopes.1


#cRewTrOrder:Distractor
Trng_GLMM_1A.slopes.2 <- emtrends(Trng_GLMM_1A, "Distractor", "cRewTrOrder",
                                  type = "response")
Trng_GLMM_1A.slopes.2
pairs(Trng_GLMM_1A.slopes.2)

# Store the plot information to plot using ggplot2
Trng_GLMM_1A.slopes.2.PlotInfo <- emmip(Trng_GLMM_1A, Distractor ~ cRewTrOrder, 
                                        at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)), 
                                        CIs = TRUE,
                                        plotit = FALSE)
      
# Plot simple slopes using ggplot
# First, we back transform the variables 
Trng_GLMM_1A.slopes.2.PlotInfo$t_yvar <- exp(Trng_GLMM_1A.slopes.2.PlotInfo$yvar)
Trng_GLMM_1A.slopes.2.PlotInfo$t_LCL <- exp(Trng_GLMM_1A.slopes.2.PlotInfo$LCL)
Trng_GLMM_1A.slopes.2.PlotInfo$t_UCL <- exp(Trng_GLMM_1A.slopes.2.PlotInfo$UCL)
Trng_GLMM_1A.slopes.2.PlotInfo$t_SE <- exp(Trng_GLMM_1A.slopes.2.PlotInfo$SE)

## Build the table for plotting scatter points that is averaged across by subjects
# Extract columns of interest: SubID, Reward, cRewTrOOrder & RT_calc
Trng_ScatterTable_2 <- cbind.data.frame(Trng_DataCompiled_Selected_Correct$SubID,
                                      Trng_DataCompiled_Selected_Correct$Distractor,
                                      Trng_DataCompiled_Selected_Correct$cRewTrOrder,
                                      Trng_DataCompiled_Selected_Correct$RT_calc)
colnames(Trng_ScatterTable_2) <- c("SubID", "Distractor", "cRewTrOrder", "RT_calc")

# Aggregate the table across cRewTrOrder & Reward 
Trng_ScatterTable_2 <- aggregate(Trng_ScatterTable_2$RT_calc, 
                               by = list(Trng_ScatterTable_2$cRewTrOrder, 
                                         Trng_ScatterTable_2$Distractor),
                               FUN = mean)
colnames(Trng_ScatterTable_2) <- c( "cRewTrOrder", "Distractor", "RT_calc")



library(ggplot2)

Plot.Trng_GLMM_1A.slopes.2 <- 
  ggplot(data = Trng_GLMM_1A.slopes.2.PlotInfo, 
         aes(x = cRewTrOrder, y = t_yvar, color = Distractor)) +
  geom_line() + 
  geom_ribbon(aes(ymax = t_yvar + t_SE, ymin = t_yvar - t_SE), alpha = 0.2,
              linetype = 0) +
  geom_jitter(data = Trng_ScatterTable_2,
              mapping = aes(x = cRewTrOrder, y = RT_calc, color = Distractor),
              alpha = 0.5, shape = 17) +
  xlab("Proportion of trials") + ylab("RT")
Plot.Trng_GLMM_1A.slopes.2

## For CoRN2023
# Rename factor levels for Distractor 
Trng_GLMM_1A.slopes.2.PlotInfo$Distractor2[
  Trng_GLMM_1A.slopes.2.PlotInfo$Distractor == "AftT"] <- "T+1"
Trng_GLMM_1A.slopes.2.PlotInfo$Distractor2[
  Trng_GLMM_1A.slopes.2.PlotInfo$Distractor == "BefT"] <- "T-1"
Trng_GLMM_1A.slopes.2.PlotInfo$Distractor <- 
  Trng_GLMM_1A.slopes.2.PlotInfo$Distractor2

Plot.Trng_GLMM_1A.slopes.2 <- 
  ggplot(data = Trng_GLMM_1A.slopes.2.PlotInfo, 
         aes(x = cRewTrOrder, y = t_yvar, color = Distractor)) +
  geom_line() + 
  geom_ribbon(aes(ymax = t_yvar + t_SE, ymin = t_yvar - t_SE), alpha = 0.2,
              linetype = 0) +
  xlab("Proportion of trials") + ylab("RT (ms)")
Plot.Trng_GLMM_1A.slopes.2

## Plot three-way interaction Reward*cRewTrOrder*Distractor
# Store the plot information to plot using ggplot2
Trng_GLMM_1A.slopes.3.PlotInfo <- emmip(Trng_GLMM_1A, 
                                        Reward ~ cRewTrOrder * Distractor, 
                                        at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)),
                                        type = "response",
                                        CIs = TRUE,
                                        plotit = FALSE)

# Rename factor levels for Distractor 
Trng_GLMM_1A.slopes.3.PlotInfo$Distractor2[
  Trng_GLMM_1A.slopes.3.PlotInfo$Distractor == "AftT"] <- "T+1"
Trng_GLMM_1A.slopes.3.PlotInfo$Distractor2[
  Trng_GLMM_1A.slopes.3.PlotInfo$Distractor == "BefT"] <- "T-1"
Trng_GLMM_1A.slopes.3.PlotInfo$Distractor <- 
  Trng_GLMM_1A.slopes.3.PlotInfo$Distractor2

Trng_GLMM_1A.slopes.3.PlotInfo.means <- emmip(Trng_GLMM_1A, 
                                        Reward ~ cRewTrOrder * Distractor, 
                                        at = list(cRewTrOrder = c(0.125,.375,.625,.875)),
                                        type = "response",
                                        CIs = TRUE,
                                        plotit = FALSE)

# Rename factor levels for Distractor 
Trng_GLMM_1A.slopes.3.PlotInfo.means$Distractor2[
  Trng_GLMM_1A.slopes.3.PlotInfo.means$Distractor == "AftT"] <- "T+1"
Trng_GLMM_1A.slopes.3.PlotInfo.means$Distractor2[
  Trng_GLMM_1A.slopes.3.PlotInfo.means$Distractor == "BefT"] <- "T-1"
Trng_GLMM_1A.slopes.3.PlotInfo.means$Distractor <- 
  Trng_GLMM_1A.slopes.3.PlotInfo.means$Distractor2



Plot.Trng_GLMM_1A.slopes.3 <- 
  ggplot(data = Trng_GLMM_1A.slopes.3.PlotInfo, 
         aes(x = cRewTrOrder, y = yvar, color = Reward)) +
  geom_line(linewidth = 0.9) + 
  geom_ribbon(aes(ymax = yvar + SE, ymin = yvar - SE), alpha = 0.1,
              linetype = 0) +
  geom_jitter(data = Trng_GLMM_1A.slopes.3.PlotInfo.means,
             mapping = aes(x = cRewTrOrder, y = yvar, color = Reward),
             shape = 17, width = 0.05, height = 0) +
  facet_wrap(~ Distractor) +
  xlab("Proportion of trials (360)") + ylab("RT (ms)") +
  theme_bw(base_size=14) +
  scale_color_hue(labels = c("High", "Low", "No"))
Plot.Trng_GLMM_1A.slopes.3

## Analyse glmer on accuracy rates
## We need to use binomial family to perform logistic regression
Trng_GLMM_2 <- glmer(ACC ~ Reward * cRewTrOrder * Distractor +
                       (1 + Reward + cRewTrOrder + Distractor | SubID),
                     data = Trng_DataCompiled_Selected, 
                     family = binomial)

Trng_GLMM_2 <- readRDS("Trng_GLMM_2.rds")

anova(Trng_GLMM_2)
Anova(Trng_GLMM_2)

# Distractor:cRewTrOrder
Trng_GLMM_2.slopes.1 <- emtrends(Trng_GLMM_2, "Distractor", "cRewTrOrder")
Trng_GLMM_2.slopes.1
pairs(Trng_GLMM_2.slopes.1)

# Store the plot information to plot using ggplot2
Trng_GLMM_2.slopes.1.PlotInfo <- emmip(Trng_GLMM_2, Distractor ~ cRewTrOrder, 
                                        at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)),
                                       type = "response",
                                        CIs = TRUE,
                                        plotit = FALSE)


## Build the table for plotting scatter points that is averaged across by subjects
# Extract columns of interest: SubID, Reward, cRewTrOOrder & RT_calc
Trng_ScatterTable_3 <- cbind.data.frame(Trng_DataCompiled_Selected$SubID,
                                        Trng_DataCompiled_Selected$Distractor,
                                        Trng_DataCompiled_Selected$cRewTrOrder,
                                        Trng_DataCompiled_Selected$ACC)
colnames(Trng_ScatterTable_3) <- c("SubID", "Distractor", "cRewTrOrder", "ACC")

# Aggregate the table across cRewTrOrder & Reward 
Trng_ScatterTable_3 <- aggregate(Trng_ScatterTable_3$ACC, 
                                 by = list(Trng_ScatterTable_3$cRewTrOrder, 
                                           Trng_ScatterTable_3$Distractor),
                                 FUN = mean)
colnames(Trng_ScatterTable_3) <- c( "cRewTrOrder", "Distractor", "ACC")

library(ggplot2)
# Previous one with wrong SE band but keeping as the scatter is useful
Plot.Trng_GLMM_2.slopes.1 <- 
  ggplot(data = Trng_GLMM_2.slopes.1.PlotInfo, 
         aes(x = cRewTrOrder, y = t_yvar, color = Distractor)) +
  geom_line() +
  geom_jitter(data = Trng_ScatterTable_3,
              mapping = aes(x = cRewTrOrder, y = ACC, color = Distractor),
              alpha = 0.5, shape = 17) +
  xlab("Proportion of trials") + ylab("Accuracy")
Plot.Trng_GLMM_2.slopes.1

## For CoRN2023
# Rename factor levels for Distractor 
Trng_GLMM_2.slopes.1.PlotInfo$Distractor2[
  Trng_GLMM_2.slopes.1.PlotInfo$Distractor == "AftT"] <- "T+1"
Trng_GLMM_2.slopes.1.PlotInfo$Distractor2[
  Trng_GLMM_2.slopes.1.PlotInfo$Distractor == "BefT"] <- "T-1"
Trng_GLMM_2.slopes.1.PlotInfo$Distractor <- 
  Trng_GLMM_2.slopes.1.PlotInfo$Distractor2

library(scales)

Plot.Trng_GLMM_2.slopes.1 <- 
  ggplot(data = Trng_GLMM_2.slopes.1.PlotInfo, 
         aes(x = cRewTrOrder, y = yvar, color = Distractor)) +
  geom_line() +
  geom_ribbon(aes(ymax = yvar + SE, ymin = yvar - SE), alpha = 0.2,
              linetype = 0) +
  xlab("Proportion of trials") + ylab("Accuracy") +
  scale_y_continuous(labels = label_number(accuracy = 0.01))
Plot.Trng_GLMM_2.slopes.1


## Plot three-way interaction Reward*cRewTrOrder*Distractor
# Store the plot information to plot using ggplot2
Trng_GLMM_2.slopes.2.PlotInfo <- emmip(Trng_GLMM_2, 
                                        Reward ~ cRewTrOrder * Distractor, 
                                        at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)),
                                        type = "response",
                                        CIs = TRUE,
                                        plotit = FALSE)

# Rename factor levels for Distractor 
Trng_GLMM_2.slopes.2.PlotInfo$Distractor2[
  Trng_GLMM_2.slopes.2.PlotInfo$Distractor == "AftT"] <- "T+1"
Trng_GLMM_2.slopes.2.PlotInfo$Distractor2[
  Trng_GLMM_2.slopes.2.PlotInfo$Distractor == "BefT"] <- "T-1"
Trng_GLMM_2.slopes.2.PlotInfo$Distractor <- 
  Trng_GLMM_2.slopes.2.PlotInfo$Distractor2

Trng_GLMM_2.slopes.2.PlotInfo.means <- emmip(Trng_GLMM_2, 
                                       Reward ~ cRewTrOrder * Distractor, 
                                       at = list(cRewTrOrder = c(0.125,.375,.625,.875)),
                                       type = "response",
                                       CIs = TRUE,
                                       plotit = FALSE)

# Rename factor levels for Distractor 
Trng_GLMM_2.slopes.2.PlotInfo.means$Distractor2[
  Trng_GLMM_2.slopes.2.PlotInfo.means$Distractor == "AftT"] <- "T+1"
Trng_GLMM_2.slopes.2.PlotInfo.means$Distractor2[
  Trng_GLMM_2.slopes.2.PlotInfo.means$Distractor == "BefT"] <- "T-1"
Trng_GLMM_2.slopes.2.PlotInfo.means$Distractor <- 
  Trng_GLMM_2.slopes.2.PlotInfo.means$Distractor2



Plot.Trng_GLMM_2.slopes.2 <- 
  ggplot(data = Trng_GLMM_2.slopes.2.PlotInfo, 
         aes(x = cRewTrOrder, y = yvar, color = Reward)) +
  geom_line(linewidth = 0.9) + 
  geom_ribbon(aes(ymax = yvar + SE, ymin = yvar - SE), alpha = 0.15,
              linetype = 0) +
  geom_jitter(data = Trng_GLMM_2.slopes.2.PlotInfo.means,
             mapping = aes(cRewTrOrder, y = yvar, color = Reward),
             shape = 17, width = 0.05, height = 0) +
  facet_wrap(~ Distractor) +
  xlab("Proportion of trials (360)") + ylab("Accuracy") +
  theme_bw(base_size=14) +
  scale_color_hue(labels = c("High", "Low", "No"))
Plot.Trng_GLMM_2.slopes.2

 ######### Start of Analysis of Testing task ######################
############### Start of Examine ACC rate and slowness trends#############
rm(list=ls())

## Read compiled RT data
Testing_DataCompiled <- read.csv('RDTC_Testing_DataCompiled_V1.csv')
Testing_ToExcludeList <- read.csv("RDTC_Testing_ToExcludeList_V1.csv")

## Organise the dataset for examining performance for excluded vs not excluded
# SubID, ToExclude, OverallACCRate, Block, AccRate

# Create data frame to compile data
PerfComp_Compiled <- data.frame(matrix(NA,    # Create empty data frame
                                       nrow = 0,
                                       ncol = 6))
colnames(PerfComp_Compiled) <- c("SubID", "ToExclude", "OverallAccRate", 
                                 "Block", "AccRate", "NumSlowMissingTrials")

for(sub in 1:dim(Testing_ToExcludeList)[1]){
  # Create empty temporary data frame to store the sub's extracted data
  # There are 5 blocks for testing phase
  PerfComp_Sub <- data.frame(matrix(NA,    # Create empty data frame
                                    nrow = 5,
                                    ncol = 6))
  colnames(PerfComp_Sub) <- c("SubID", "ToExclude", "OverallAccRate", 
                              "Block", "AccRate", "NumSlowMissingTrials")
  # Extract pt's raw data
  SubData <- Testing_DataCompiled[Testing_DataCompiled$SubID == 
                                    Testing_ToExcludeList$SubID[sub],]
  
  PerfComp_Sub$SubID <- Testing_ToExcludeList$SubID[sub]
  PerfComp_Sub$ToExclude <- Testing_ToExcludeList$ToExclude[sub]
  
  # Calculate overall accuracy rate
  PerfComp_Sub$OverallAccRate <- mean(SubData$ACC)
  
  # Calculate mean accuracy for each block
  # For training phase, there are 5 blocks
  # Each block, there are 36 trials
  for (bl in 1:5){
    PerfComp_Sub$Block[bl] <- bl
    PerfComp_Sub$AccRate[bl] <- mean(SubData$ACC[SubData$Block == bl])
    PerfComp_Sub$NumSlowMissingTrials[bl] <- 
      36 - sum(SubData$ACC[SubData$Block == bl])
  }
  
  # compile into PerfComp_Compiled df
  PerfComp_Compiled <- rbind(PerfComp_Compiled, PerfComp_Sub)
}  

library(ggplot2)

ggplot(data = PerfComp_Compiled, 
       aes(x = Block, y = AccRate, colour = ToExclude)) +
  geom_point() +
  geom_smooth(method = "loess")

ggplot(data = PerfComp_Compiled, 
       aes(x = Block, y = NumSlowMissingTrials, colour = ToExclude)) +
  geom_point() +
  geom_smooth(method = "loess")
############### End of Examine ACC rate and slowness trends#############

############## Start of formal data analysis ###############
rm(list=ls())

## Read compiled RT data
Testing_DataCompiled <- read.csv('RDTC_Testing_DataCompiled_V1.csv')
Testing_ToExcludeList <- read.csv("RDTC_Testing_ToExcludeList_V1.csv")

## Create new data frame to select pt's data
Testing_DataCompiled_Selected <- data.frame(matrix(NA,    # Create empty data frame
                                                   nrow = 0,
                                                   ncol = 17))
colnames(Testing_DataCompiled_Selected) <- c("X","SubID", "TrialNum", "Targ", "TargPos", "Group", 
                                             "Reward", "RESP", "ACC", 
                                             "RT_FromFramePress", "RESP_Frame",
                                             "Block", "BlkTrialNum", "RewTrOrder", "Distractor",
                                             "RESP_Frame_Num", "RT_Calc")

## Subset Testing_ToExcludeList_V1 to select ToExclude == No
Testing_ToExcludeList_V1_Selected <- subset(Testing_ToExcludeList, 
                                            ToExclude == "No")
for(sub in 1:dim(Testing_ToExcludeList_V1_Selected)[1]){
  DataSelect <- Testing_DataCompiled[
    Testing_DataCompiled$SubID == Testing_ToExcludeList_V1_Selected$SubID[sub],]
  Testing_DataCompiled_Selected <- rbind(Testing_DataCompiled_Selected, DataSelect)
}

## Exclude participants from traning phase who were identified as outliers
## 647311 651845 654825 662953
Trng_ExclPts <- c(647311, 651845, 654825, 662953)
for(sub in 1:length(Trng_ExclPts)){
  Testing_DataCompiled_Selected <- 
    subset(Testing_DataCompiled_Selected, SubID != Trng_ExclPts[sub])
}

## Examine sample characteristics
# Count total number of pts included in analysis
length(unique(Testing_DataCompiled_Selected$SubID))

#Count number of pts in each counterbalance group
length(unique(Testing_DataCompiled_Selected$SubID[
  Testing_DataCompiled_Selected$Group == 1]))
length(unique(Testing_DataCompiled_Selected$SubID[
  Testing_DataCompiled_Selected$Group == 2]))
length(unique(Testing_DataCompiled_Selected$SubID[
  Testing_DataCompiled_Selected$Group == 3]))

## Plot histogram to examine distribution
library(ggplot2)

Testing_RT_histo <- ggplot(Testing_DataCompiled_Selected, aes(x = RT_calc)) 
Testing_RT_histo + geom_histogram()

## Fit RT by Reward Trial Number based based on reward condition
ggplot(data = Testing_DataCompiled_Selected, 
       aes(RewTrOrder, RT_calc, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "loess")

## Fit RT by RewTrOrder with regression line
ggplot(data = Testing_DataCompiled_Selected, 
       aes(RewTrOrder, RT_calc, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "lm")

## Fit ACC by Reward Trial Number based based on reward condition
ggplot(data = Testing_DataCompiled_Selected, 
       aes(RewTrOrder, ACC, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "loess")

ggplot(data = Testing_DataCompiled_Selected, 
       aes(RewTrOrder, ACC, colour = Reward)) + facet_wrap("Distractor") +
  geom_smooth(method = "lm")

Testing_DataCompiled_Selected$SubID <- as.factor(Testing_DataCompiled_Selected$SubID)
Testing_DataCompiled_Selected$Distractor <- as.factor(Testing_DataCompiled_Selected$Distractor)
Testing_DataCompiled_Selected$Reward <- as.factor(Testing_DataCompiled_Selected$Reward)

## Make RewTrOrder scale from 0 to 1 
## Since RewTrOrder are 1: 60 we'll divide everything by 60
Testing_DataCompiled_Selected$cRewTrOrder <- 
  Testing_DataCompiled_Selected$RewTrOrder / 60

library(lme4)
library(afex)
library(car)

Testing_GLMM_1 <- glmer(RT_calc ~ Reward * cRewTrOrder * Distractor +
                       (1 + Reward + cRewTrOrder + Distractor | SubID),
                     data = Testing_DataCompiled_Selected, 
                     subset = (ACC == 1),
                     family = Gamma(link = "log")) # fitted with convergence warning

checkFit_Testing_GLMM_1 <- allFit(Testing_GLMM_1)

checkFit_Testing_GLMM_1.ok <- 
  checkFit_Testing_GLMM_1[sapply(checkFit_Testing_GLMM_1, is, "merMod")]
lapply(checkFit_Testing_GLMM_1.ok, function(x)   
  x@optinfo$conv$lme4$messages) # bobyqa & nlminbwrap fitted without warnings

# Refit with bobyqa optimiser
Testing_GLMM_1A <- update(Testing_GLMM_1,
                          control=glmerControl(optimizer="bobyqa",
                                                        optCtrl=list(maxfun=2e5))) # converged successfully


Testing_GLMM_1A <- readRDS("Testing_GLMM_1A.rds")

anova(Testing_GLMM_1A)
Anova(Testing_GLMM_1A)


library(emmeans)

## Probe cRewTrOrder:Distractor sig interaction
Testing_GLMM_1.slopes.1 <- emtrends(Testing_GLMM_1A, "Distractor", "cRewTrOrder")
Testing_GLMM_1.slopes.1
pairs(Testing_GLMM_1.slopes.1)

# Store the plot information to plot using ggplot2
Testing_GLMM_1.slopes.1.PlotInfo <- emmip(Testing_GLMM_1A, Distractor ~ cRewTrOrder, 
                                        at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)),
                                        type = "response",
                                        CIs = TRUE,
                                        plotit = FALSE)

## Build the table for plotting scatter points that is averaged across by subjects
# Extract columns of interest: SubID, Reward, cRewTrOOrder & RT_calc for correct responses only
Testing_DataCompiled_Selected_Correct <- subset(Testing_DataCompiled_Selected, ACC == 1)

Testing_ScatterTable <- cbind.data.frame(Testing_DataCompiled_Selected_Correct$SubID,
                                        Testing_DataCompiled_Selected_Correct$Distractor,
                                        Testing_DataCompiled_Selected_Correct$cRewTrOrder,
                                        Testing_DataCompiled_Selected_Correct$RT_calc)
colnames(Testing_ScatterTable) <- c("SubID", "Distractor", "cRewTrOrder", "RT_calc")

# Aggregate the table across cRewTrOrder & Reward 
Testing_ScatterTable <- aggregate(Testing_ScatterTable$RT_calc, 
                                 by = list(Testing_ScatterTable$cRewTrOrder, 
                                           Testing_ScatterTable$Distractor),
                                 FUN = mean)
colnames(Testing_ScatterTable) <- c( "cRewTrOrder", "Distractor", "RT_calc")

library(ggplot2)

# Rename factor levels for Distractor 
Testing_GLMM_1.slopes.1.PlotInfo$Distractor2[
  Testing_GLMM_1.slopes.1.PlotInfo$Distractor == "AftT"] <- "T+1"
Testing_GLMM_1.slopes.1.PlotInfo$Distractor2[
  Testing_GLMM_1.slopes.1.PlotInfo$Distractor == "BefT"] <- "T-1"
Testing_GLMM_1.slopes.1.PlotInfo$Distractor <- 
  Testing_GLMM_1.slopes.1.PlotInfo$Distractor2

Plot.Testing_GLMM_1.slopes.1 <- 
  ggplot(data = Testing_GLMM_1.slopes.1.PlotInfo, 
         aes(x = cRewTrOrder, y = yvar, color = Distractor)) +
  geom_line() + 
  geom_ribbon(aes(ymax = yvar + SE, ymin = yvar - SE), alpha = 0.2,
              linetype = 0) +
  geom_jitter(data = Testing_ScatterTable,
              mapping = aes(x = cRewTrOrder, y = RT_calc, color = Distractor),
              alpha = 0.5, shape = 17) +
  xlab("Proportion of trials") + ylab("RT (ms)")
Plot.Testing_GLMM_1.slopes.1

# For CoRN2023
Plot.Testing_GLMM_1.slopes.1 <- 
  ggplot(data = Testing_GLMM_1.slopes.1.PlotInfo, 
         aes(x = cRewTrOrder, y = yvar, color = Distractor)) +
  geom_line() + 
  geom_ribbon(aes(ymax = yvar + SE, ymin = yvar - SE), alpha = 0.2,
              linetype = 0) +
  xlab("Proportion of trials") + ylab("RT (ms)")
Plot.Testing_GLMM_1.slopes.1

## Plot the 3-way interaction
# Store the plot information to plot using ggplot2
Testing_GLMM_1.slopes.2.PlotInfo <- emmip(Testing_GLMM_1A, 
                                          Distractor ~ cRewTrOrder * Reward, 
                                          at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)),
                                          type = "response",
                                          CIs = TRUE,
                                          plotit = FALSE)

# Rename factor levels for Distractor 
Testing_GLMM_1.slopes.2.PlotInfo$Distractor2[
  Testing_GLMM_1.slopes.2.PlotInfo$Distractor == "AftT"] <- "T+1"
Testing_GLMM_1.slopes.2.PlotInfo$Distractor2[
  Testing_GLMM_1.slopes.2.PlotInfo$Distractor == "BefT"] <- "T-1"
Testing_GLMM_1.slopes.2.PlotInfo$Distractor <- 
  Testing_GLMM_1.slopes.2.PlotInfo$Distractor2

Testing_GLMM_1.slopes.2.PlotInfo.means <- emmip(Testing_GLMM_1A, 
                                          Distractor ~ cRewTrOrder * Reward, 
                                          at = list(cRewTrOrder = c(0.125,.375,.625,.875)),
                                          type = "response",
                                          CIs = TRUE,
                                          plotit = FALSE)

# Rename factor levels for Distractor 
Testing_GLMM_1.slopes.2.PlotInfo.means$Distractor2[
  Testing_GLMM_1.slopes.2.PlotInfo.means$Distractor == "AftT"] <- "T+1"
Testing_GLMM_1.slopes.2.PlotInfo.means$Distractor2[
  Testing_GLMM_1.slopes.2.PlotInfo.means$Distractor == "BefT"] <- "T-1"
Testing_GLMM_1.slopes.2.PlotInfo.means$Distractor <- 
  Testing_GLMM_1.slopes.2.PlotInfo.means$Distractor2


Plot.Testing_GLMM_1.slopes.2 <- 
  ggplot(data = Testing_GLMM_1.slopes.2.PlotInfo, 
         aes(x = cRewTrOrder, y = yvar, color = Reward)) +
  geom_line(linewidth = 0.9) + 
  geom_ribbon(aes(ymax = yvar + SE, ymin = yvar - SE), alpha = 0.1,
              linetype = 0) +
  geom_jitter(data = Testing_GLMM_1.slopes.2.PlotInfo.means,
             mapping = aes(cRewTrOrder, y = yvar, color = Reward),
             shape = 17, width = 0.05, height = 0) +
  facet_wrap(~ Distractor) +
  xlab("Proportion of trials (180)") + ylab("RT (ms)") +
  theme_bw(base_size=14) +
  scale_color_hue(labels = c("High", "Low", "No"))
Plot.Testing_GLMM_1.slopes.2

## Analyse glmer on accuracy rates
## We need to use binomial family to perform logistic regression
Testing_GLMM_2 <- glmer(ACC ~ Reward * cRewTrOrder * Distractor +
                       (1 + Reward + cRewTrOrder + Distractor | SubID),
                     data = Testing_DataCompiled_Selected,
                     family = binomial) # fitted with isSingular warning

checkFit_Testing_GLMM_2 <- allFit(Testing_GLMM_2)

checkFit_Testing_GLMM_2.ok <- 
  checkFit_Testing_GLMM_2[sapply(checkFit_Testing_GLMM_2, is, "merMod")]
lapply(checkFit_Testing_GLMM_2.ok, function(x)   
  x@optinfo$conv$lme4$messages) # All optimisations failed to converge successfully

# Reduce model specification and refit
# Random slopes only for time
Testing_GLMM_2A <- glmer(ACC ~ Reward * cRewTrOrder * Distractor +
                          (1 +  cRewTrOrder | SubID),
                        data = Testing_DataCompiled_Selected,
                        family = binomial) # still fitted with convergence warning

# Further simplify the model
Testing_GLMM_2B <- glmer(ACC ~ Reward * cRewTrOrder * Distractor +
                           (1 | SubID),
                         data = Testing_DataCompiled_Selected,
                         family = binomial) # still fitted with convergence warning
# try refitting with optimisers
checkFit_Testing_GLMM_2B <- allFit(Testing_GLMM_2B)

checkFit_Testing_GLMM_2B.ok <- 
  checkFit_Testing_GLMM_2B[sapply(checkFit_Testing_GLMM_2B, is, "merMod")]
lapply(checkFit_Testing_GLMM_2B.ok, function(x)   
  x@optinfo$conv$lme4$messages) #bobyqa & nlimbwrap fitted without warnings

# refit using bobyqa
Testing_GLMM_2B <- update(Testing_GLMM_2B,
                          control=glmerControl(optimizer="bobyqa",
                                               optCtrl=list(maxfun=2e5))) # fitted successfully

Testing_GLMM_2B <- readRDS("Testing_GLMM_2B.rds")

anova(Testing_GLMM_2B)
Anova(Testing_GLMM_2B)

## Probe the significant Distractor main effect
emmeans(Testing_GLMM_2B, "Distractor", type = "response")

## Plot the 3-way interaction
# Store the plot information to plot using ggplot2
Testing_GLMM_2.slopes.1.PlotInfo <- emmip(Testing_GLMM_2B, 
                                          Distractor ~ cRewTrOrder * Reward, 
                                          at = list(cRewTrOrder = c(0,0.2,0.4,0.6,0.8,1.0)),
                                          type = "response",
                                          CIs = TRUE,
                                          plotit = FALSE)

# Rename factor levels for Distractor 
Testing_GLMM_2.slopes.1.PlotInfo$Distractor2[
  Testing_GLMM_2.slopes.1.PlotInfo$Distractor == "AftT"] <- "T+1"
Testing_GLMM_2.slopes.1.PlotInfo$Distractor2[
  Testing_GLMM_2.slopes.1.PlotInfo$Distractor == "BefT"] <- "T-1"
Testing_GLMM_2.slopes.1.PlotInfo$Distractor <- 
  Testing_GLMM_2.slopes.1.PlotInfo$Distractor2

Testing_GLMM_2.slopes.1.PlotInfo.means <- emmip(Testing_GLMM_2B, 
                                          Distractor ~ cRewTrOrder * Reward, 
                                          at = list(cRewTrOrder = c(0.125,.375,.625,.875)),
                                          type = "response",
                                          CIs = TRUE,
                                          plotit = FALSE)

# Rename factor levels for Distractor 
Testing_GLMM_2.slopes.1.PlotInfo.means$Distractor2[
  Testing_GLMM_2.slopes.1.PlotInfo.means$Distractor == "AftT"] <- "T+1"
Testing_GLMM_2.slopes.1.PlotInfo.means$Distractor2[
  Testing_GLMM_2.slopes.1.PlotInfo.means$Distractor == "BefT"] <- "T-1"
Testing_GLMM_2.slopes.1.PlotInfo.means$Distractor <- 
  Testing_GLMM_2.slopes.1.PlotInfo.means$Distractor2

library(scales)

Plot.Testing_GLMM_2.slopes.1 <- 
  ggplot(data = Testing_GLMM_2.slopes.1.PlotInfo, 
         aes(x = cRewTrOrder, y = yvar, color = Reward)) +
  geom_line(linewidth = 0.9) + 
  geom_ribbon(aes(ymax = yvar + SE, ymin = yvar - SE), alpha = 0.1,
              linetype = 0) +
  geom_jitter(data = Testing_GLMM_2.slopes.1.PlotInfo.means,
             mapping = aes(x = cRewTrOrder, y = yvar, color = Reward),
             shape = 17, width = 0.05, height = 0) +
  facet_wrap(~ Distractor) +
  xlab("Proportion of trials (180)") + ylab("Accuracy") +
  theme_bw(base_size=14) +
  scale_color_hue(labels = c("High", "Low", "No")) +
  scale_y_continuous(labels = label_number(accuracy = 0.01))
Plot.Testing_GLMM_2.slopes.1
