RABT is Experiment 1 of Jingwen's PhD project.
This is a skeleton AB task with T2s with reward manipulation.

- RABT_TrialListGen.m is the matlab script to generate trial lists for the task. 
- The csv files that are labelled "TrialList_Group_Block" are trial lists that are generated.
- Column info:
-- Group is 1 to 6 as there are 6 unique counterbalanced orders for 3 levels of reward
-- Reward: 1=noreward, 2=lowreward, 3=highreward
-- SOA: 6 = 100ms (6frames), 36 = 600 ms (36 frames)
-- T1 and T2 are letters randomly selected, D1 and D2 are digits randomly selected
-- T2col is the colour that is assigned to T2 depending on the Group number (i.e. first column)
-- Block is block number. Altogether the task is split into 5 blocks, a break between each one.
-- TrialNum counts the trial/video that is being presented to facilitate us to track.


- RABT.psyexp and RABT_lastrun.py are Jingwen's skeletal AB code created using PsychoPy builder
-- The code works to present all the trials for a selected trial list inside the same folder. See above for more info for TrialLists and crossers to writeup for the design.
-- This code is for EXPT1's training phase and once confirm that it works fine, we can easily adapt for all the other experiments' code.