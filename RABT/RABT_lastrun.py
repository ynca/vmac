﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2020.1.2),
    on Wed Sep 15 18:27:07 2021
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2020.1.2'
expName = 'RABT'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001'}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/RABT_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=[800,600], fullscr=False, screen=0, 
    winType='pyglet', allowGUI=True, allowStencil=False,
    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "Ready"
ReadyClock = core.Clock()
text_2 = visual.TextStim(win=win, name='text_2',
    text='Get ready, the trials will begin shortly.',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "trial"
trialClock = core.Clock()
Fixation = visual.TextStim(win=win, name='Fixation',
    text='+',
    font='Courier New',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
T1_Disp = visual.TextStim(win=win, name='T1_Disp',
    text='default text',
    font='Courier New',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
D1_Disp = visual.TextStim(win=win, name='D1_Disp',
    text='default text',
    font='Courier New',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-3.0);
SOA_Disp = visual.TextStim(win=win, name='SOA_Disp',
    text=None,
    font='Courier New',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
T2_Disp = visual.TextStim(win=win, name='T2_Disp',
    text='default text',
    font='Courier New',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-5.0);
D2_Disp = visual.TextStim(win=win, name='D2_Disp',
    text='default text',
    font='Courier New',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-6.0);

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "Ready"-------
continueRoutine = True
# update component parameters for each repeat
# keep track of which components have finished
ReadyComponents = [text_2]
for thisComponent in ReadyComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
ReadyClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Ready"-------
while continueRoutine:
    # get current time
    t = ReadyClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=ReadyClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_2* updates
    if text_2.status == NOT_STARTED and frameN >= 0.0:
        # keep track of start time/frame for later
        text_2.frameNStart = frameN  # exact frame index
        text_2.tStart = t  # local t and not account for scr refresh
        text_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text_2, 'tStartRefresh')  # time at next scr refresh
        text_2.setAutoDraw(True)
    if text_2.status == STARTED:
        if frameN >= (text_2.frameNStart + 120):
            # keep track of stop time/frame for later
            text_2.tStop = t  # not accounting for scr refresh
            text_2.frameNStop = frameN  # exact frame index
            win.timeOnFlip(text_2, 'tStopRefresh')  # time at next scr refresh
            text_2.setAutoDraw(False)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ReadyComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Ready"-------
for thisComponent in ReadyComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('text_2.started', text_2.tStartRefresh)
thisExp.addData('text_2.stopped', text_2.tStopRefresh)
# the Routine "Ready" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials = data.TrialHandler(nReps=1, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('Triallist_Group1_Block1.csv'),
    seed=None, name='trials')
thisExp.addLoop(trials)  # add the loop to the experiment
thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
if thisTrial != None:
    for paramName in thisTrial:
        exec('{} = thisTrial[paramName]'.format(paramName))

for thisTrial in trials:
    currentLoop = trials
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec('{} = thisTrial[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "trial"-------
    continueRoutine = True
    # update component parameters for each repeat
    import random
    import string
    
    #Set duration of objects
    #100ms = 6frames
    #SOADur draws the pre-assigned SOA duration from the trial list
    FixDur= randint(18,42)
    T1Dur = 6 #6
    D1Dur = 6 #6
    SOADur = SOA
    T2Dur = 6 #6
    D2Dur = 6 #6
    
    T1StartTime = FixDur 
    D1StartTime = FixDur + T1Dur
    SOAStartTime = FixDur + T1Dur + D1Dur
    T2StartTime = FixDur + T1Dur + D1Dur + SOADur
    D2StartTime = FixDur + T1Dur + D1Dur + SOADur + T2Dur
    D2EndTime = FixDur + T1Dur + D1Dur + SOADur + T2Dur + D2Dur
    
    
    
    T1_Disp.setText(T1)
    D1_Disp.setText(D1)
    T2_Disp.setColor(T2Col, colorSpace='rgb')
    T2_Disp.setText(T2)
    D2_Disp.setText(D2)
    # keep track of which components have finished
    trialComponents = [Fixation, T1_Disp, D1_Disp, SOA_Disp, T2_Disp, D2_Disp]
    for thisComponent in trialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "trial"-------
    while continueRoutine:
        # get current time
        t = trialClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=trialClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *Fixation* updates
        if Fixation.status == NOT_STARTED and frameN >= 0.0:
            # keep track of start time/frame for later
            Fixation.frameNStart = frameN  # exact frame index
            Fixation.tStart = t  # local t and not account for scr refresh
            Fixation.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(Fixation, 'tStartRefresh')  # time at next scr refresh
            Fixation.setAutoDraw(True)
        if Fixation.status == STARTED:
            if frameN >= (Fixation.frameNStart + FixDur):
                # keep track of stop time/frame for later
                Fixation.tStop = t  # not accounting for scr refresh
                Fixation.frameNStop = frameN  # exact frame index
                win.timeOnFlip(Fixation, 'tStopRefresh')  # time at next scr refresh
                Fixation.setAutoDraw(False)
        
        # *T1_Disp* updates
        if T1_Disp.status == NOT_STARTED and frameN >= T1StartTime:
            # keep track of start time/frame for later
            T1_Disp.frameNStart = frameN  # exact frame index
            T1_Disp.tStart = t  # local t and not account for scr refresh
            T1_Disp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(T1_Disp, 'tStartRefresh')  # time at next scr refresh
            T1_Disp.setAutoDraw(True)
        if T1_Disp.status == STARTED:
            if frameN >= (T1_Disp.frameNStart + T1Dur):
                # keep track of stop time/frame for later
                T1_Disp.tStop = t  # not accounting for scr refresh
                T1_Disp.frameNStop = frameN  # exact frame index
                win.timeOnFlip(T1_Disp, 'tStopRefresh')  # time at next scr refresh
                T1_Disp.setAutoDraw(False)
        
        # *D1_Disp* updates
        if D1_Disp.status == NOT_STARTED and frameN >= D1StartTime:
            # keep track of start time/frame for later
            D1_Disp.frameNStart = frameN  # exact frame index
            D1_Disp.tStart = t  # local t and not account for scr refresh
            D1_Disp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(D1_Disp, 'tStartRefresh')  # time at next scr refresh
            D1_Disp.setAutoDraw(True)
        if D1_Disp.status == STARTED:
            if frameN >= (D1_Disp.frameNStart + D1Dur):
                # keep track of stop time/frame for later
                D1_Disp.tStop = t  # not accounting for scr refresh
                D1_Disp.frameNStop = frameN  # exact frame index
                win.timeOnFlip(D1_Disp, 'tStopRefresh')  # time at next scr refresh
                D1_Disp.setAutoDraw(False)
        
        # *SOA_Disp* updates
        if SOA_Disp.status == NOT_STARTED and frameN >= SOAStartTime:
            # keep track of start time/frame for later
            SOA_Disp.frameNStart = frameN  # exact frame index
            SOA_Disp.tStart = t  # local t and not account for scr refresh
            SOA_Disp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(SOA_Disp, 'tStartRefresh')  # time at next scr refresh
            SOA_Disp.setAutoDraw(True)
        if SOA_Disp.status == STARTED:
            if frameN >= (SOA_Disp.frameNStart + SOADur):
                # keep track of stop time/frame for later
                SOA_Disp.tStop = t  # not accounting for scr refresh
                SOA_Disp.frameNStop = frameN  # exact frame index
                win.timeOnFlip(SOA_Disp, 'tStopRefresh')  # time at next scr refresh
                SOA_Disp.setAutoDraw(False)
        
        # *T2_Disp* updates
        if T2_Disp.status == NOT_STARTED and frameN >= T2StartTime:
            # keep track of start time/frame for later
            T2_Disp.frameNStart = frameN  # exact frame index
            T2_Disp.tStart = t  # local t and not account for scr refresh
            T2_Disp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(T2_Disp, 'tStartRefresh')  # time at next scr refresh
            T2_Disp.setAutoDraw(True)
        if T2_Disp.status == STARTED:
            if frameN >= (T2_Disp.frameNStart + T2Dur):
                # keep track of stop time/frame for later
                T2_Disp.tStop = t  # not accounting for scr refresh
                T2_Disp.frameNStop = frameN  # exact frame index
                win.timeOnFlip(T2_Disp, 'tStopRefresh')  # time at next scr refresh
                T2_Disp.setAutoDraw(False)
        
        # *D2_Disp* updates
        if D2_Disp.status == NOT_STARTED and frameN >= D2StartTime:
            # keep track of start time/frame for later
            D2_Disp.frameNStart = frameN  # exact frame index
            D2_Disp.tStart = t  # local t and not account for scr refresh
            D2_Disp.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(D2_Disp, 'tStartRefresh')  # time at next scr refresh
            D2_Disp.setAutoDraw(True)
        if D2_Disp.status == STARTED:
            if frameN >= (D2_Disp.frameNStart + D2Dur):
                # keep track of stop time/frame for later
                D2_Disp.tStop = t  # not accounting for scr refresh
                D2_Disp.frameNStop = frameN  # exact frame index
                win.timeOnFlip(D2_Disp, 'tStopRefresh')  # time at next scr refresh
                D2_Disp.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "trial"-------
    for thisComponent in trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('Fixation.started', Fixation.tStartRefresh)
    trials.addData('Fixation.stopped', Fixation.tStopRefresh)
    trials.addData('T1_Disp.started', T1_Disp.tStartRefresh)
    trials.addData('T1_Disp.stopped', T1_Disp.tStopRefresh)
    trials.addData('D1_Disp.started', D1_Disp.tStartRefresh)
    trials.addData('D1_Disp.stopped', D1_Disp.tStopRefresh)
    trials.addData('SOA_Disp.started', SOA_Disp.tStartRefresh)
    trials.addData('SOA_Disp.stopped', SOA_Disp.tStopRefresh)
    trials.addData('T2_Disp.started', T2_Disp.tStartRefresh)
    trials.addData('T2_Disp.stopped', T2_Disp.tStopRefresh)
    trials.addData('D2_Disp.started', D2_Disp.tStartRefresh)
    trials.addData('D2_Disp.stopped', D2_Disp.tStopRefresh)
    # the Routine "trial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 1 repeats of 'trials'


# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
