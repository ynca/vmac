% This script is for creating trial lists for RABT task
% By Jingwen Chai
% V1, 8 Sep 2021

%% Initialise workspace
clear all; clc;
cd('/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal');

%% Trial information
% Reward, 3 levels, high (3), low (2), no reward (1)
% SOA, 2 levels, 48 frames (800ms), 18 frames (300ms)
% Group, 6 groups to counterbalance reward colours, so 6 trial list CSVs
% Block 5 blocks
% Total number of trials is 3 * 2 * 5 * 30 = 900

%% Plan
% Create 1 block of 20 trials
% Clone into 5 blocks
% For each block assign stimuli identities

%% 

% Define conditions
Cond1 = [1,6]; % No reward, low SOA
Cond2 = [1,36]; % No reward, high SOA
Cond3 = [2,6]; % Low reward, low SOA
Cond4 = [2,36]; % Low reward, high SOA
Cond5 = [3,6]; % High reward, low SOA
Cond6 = [3,36]; % High reward, high SOA

% Define T1 & T2, D1 & D2

Letters = 'ABCEFGHJKLMNPRUVWXY';
Letters = sprintfc('%c',Letters);
%Letters = shuffle(Letters);

Digits = [2,3,4,6,7,9];

%% Import spreadsheet with assigned colour mappings for each group
% Import the data
[~, ~, raw] = xlsread('RewardCounterbalanceChart.xlsx','Sheet1');
raw = raw(2:end,:);
raw(cellfun(@(x) ~isempty(x) && isnumeric(x) && isnan(x),raw)) = {''};
cellVectors = raw(:,[2,3,4]);
raw = raw(:,1);

% Create output variable
data = reshape([raw{:}],size(raw));

% Create table
GroupColour = table;

% Allocate imported array to column variable names
GroupColour.Group = data(:,1);
GroupColour.HighR = cellVectors(:,1);
GroupColour.LowR = cellVectors(:,2);
GroupColour.NoR= cellVectors(:,3);

% Clear temporary variables
clearvars data raw cellVectors;

%%
for grp = 1:6
    for blk = 1:5
    % Create block of 30
    % Col1=group, Col2=Reward, Col3=SOA
    TrialBlock = [repmat(Cond1,30,1); repmat(Cond2,30,1); repmat(Cond3,30,1); ...
        repmat(Cond4,30,1); repmat(Cond5,30,1); repmat(Cond6,30,1)];
    TrialBlock = [repmat(grp,180,1), TrialBlock];
    
    
    TrialBlock = array2table(TrialBlock, 'VariableNames', {'Group','Reward', 'SOA'});
    
    % Assign T1, T2, D1, D2 into TrialBlock
    for tr = 1:height(TrialBlock)
        Letters = shuffle(Letters);
        Digits = shuffle(Digits);
        T1 = char(Letters(1));
        T2 = char(Letters(2));
        D1 = Digits(1);
        D2 = Digits(2);
        
        TrialBlock.T1(tr) = T1;
        TrialBlock.T2(tr) = T2;
        TrialBlock.D1(tr) = D1;
        TrialBlock.D2(tr) = D2;
    end
    
    % Assign T2 colour based on Group
    TrialBlock.T2Col(TrialBlock.Reward == 1,1) =  GroupColour.NoR(grp);
    TrialBlock.T2Col(TrialBlock.Reward == 2,1) =  GroupColour.LowR(grp);
    TrialBlock.T2Col(TrialBlock.Reward == 3,1) =  GroupColour.HighR(grp);
    
    % Assign block number
    TrialBlock.Block = repmat(blk,180,1);
    
    % Random shuffle the trial block
    TrialBlock = TrialBlock(randperm(size(TrialBlock,1)), :);
    
    %Add Trial numbers to the trial list 
    TrialBlock.TrialNum = (1:height(TrialBlock))';
    
    %Set file name
    filename = strcat('Triallist_','Group', num2str(grp), '_', 'Block', ...
        num2str(blk));
    
    % Write trial list csv
    writetable(TrialBlock, strcat(filename,'.csv'), 'Delimiter', ',');
    end
end