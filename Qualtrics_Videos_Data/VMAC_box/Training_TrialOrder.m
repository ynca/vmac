% This script is to organise trial list for qualtrics
% By Jingwen Chai
% This script for files in the folder VMAC_box
% V1, 828 Sep 2021

%% Initialise workspace
clear all; clc;
cd('/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/VMAC_Videos/VMAC_box');

%% Import csv that contains target & distractor identities
filename = '/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/VMAC_Videos/VMAC_box/2021_12_14_1115_Training_updated.csv';
TrList = readtable(filename);

%% Import Trial Order csv
filename = '/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/VMAC_Videos/VMAC_box/training_trial_order_updated.csv';
TrOrder = readtable(filename);

%% Add target, distractor identities & other information to Trial Order

% Sort TrOrder by SpTr & SpNum
TrOrder = sortrows(TrOrder, {'SpTr', 'SpNum'});

% Sort TrList by trialtrype & trialnumber
TrList = sortrows(TrList, {'trialType', 'trialNumber'});

% Add columns from TrList to TrOrder
TrOrder.T1 = TrList.T1;
TrOrder.D1 = TrList.D1;
TrOrder.T2 = TrList.T2;
TrOrder.D2 = TrList.D2;
TrOrder.RSVPlength = TrList.RSVPLength;
TrOrder.fixationduration = TrList.fixationDuration;
TrOrder.RSVPduration = TrList.RSVPDuration;
TrOrder.videoduration = TrList.videoDuration;

%% !!!!!!!! DO NOT USE EVALUATE CURRENT SECTION FOR THIS SECTION !!!!!!!!!
% Define reward factor based on counterbalancing
% Import spreadsheet with counterbalancing info
CounterB = readtable('RewardCounterbalanceChart_V2.xlsx');
%Duplicate groups for short SOA and long SOA conditions
CounterB.Group(4:6) = (1:3)';
% Rename colours to corresponding conditions
CounterB(1,[2:4]) = cell2table({'RP2', 'BP2', 'GP2'});
CounterB(2,[2:4]) = cell2table({'GP2', 'RP2', 'BP2'});
CounterB(3,[2:4]) = cell2table({'BP2', 'GP2', 'RP2'});
CounterB(4,[2:4]) = cell2table({'RP8', 'BP8', 'GP8'});
CounterB(5,[2:4]) = cell2table({'GP8', 'RP8', 'BP8'});
CounterB(6,[2:4]) = cell2table({'BP8', 'GP8', 'RP8'});
% Sort table 
CounterB = sortrows(CounterB, 'Group');

% !!!! Do not proceed until Group number is updated !!!!

%Identify current group we want to use to update TrOrder
Group = 3; % Change to group number we want 1,2or3

CounterB_Current = CounterB(CounterB.Group == Group,:);
RewName = {'HR', 'LR', 'NR'};

% Create variable with empty cells to assign strings
TrOrder.Reward = cell(360,1);
% Add in reward labels 
for row = 1:height(CounterB_Current)
    for col = 2:4
        for j = 1:height(TrOrder)
            if strcmp(char(TrOrder.SpTr(j)), ... 
                    char(table2cell(CounterB_Current(row,col)))) == 1
                TrOrder.Reward(j) = RewName(col-1);
            end    
        end
    end    
end

%%

% Add in column with name for qualtrics
% Create variable with empty cells to assign strings
TrOrder.LinkName = cell(120,1);

% Assign names
for row2 = 1:height(TrOrder)
    if strcmp(char(TrOrder.SpTr(row2)),'BP2') == 1 || ... 
            strcmp(char(TrOrder.SpTr(row2)),'RP2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)),'GP2') == 1 
        TrOrder.LinkName(row2) = {strcat('lag2/', ...
            char(TrOrder.trial_type(row2)))}; 
    elseif strcmp(char(TrOrder.SpTr(row2)),'BP8') == 1 || ... 
            strcmp(char(TrOrder.SpTr(row2)),'RP8') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)),'GP8') == 1 
        TrOrder.LinkName(row2) = {strcat('lag8/', ...
            char(TrOrder.trial_type(row2)))}; 
    else
        TrOrder.LinkName(row2) = TrOrder.LinkName(row2);
    end
end

% Create variable with empty cells to assign strings
TrOrder.Colour = cell(360,1);

% Assign names for T2 colour conditions
for row2 = 1:height(TrOrder)
    if strcmp(char(TrOrder.SpTr(row2)), 'BP2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)), 'BP8') == 1 
        TrOrder.Colour(row2) = {'blue'};
    elseif strcmp(char(TrOrder.SpTr(row2)), 'RP2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)), 'RP8') == 1 
        TrOrder.Colour(row2) = {'red'};
    elseif strcmp(char(TrOrder.SpTr(row2)), 'GP2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)), 'GP8') == 1 
        TrOrder.Colour(row2) = {'green'};
    else
        TrOrder.Colour(row2) = TrOrder.Colour(row2);
    end
end


%Sort TrOrder by trial_number
TrOrder = sortrows(TrOrder, 'trial_number');

%%
% Write csv
writetable(TrOrder, strcat('Training_TrialOrder_Full_Group', ...
    num2str(Group),'.csv'),'Delimiter', ',');

writetable(TrOrder, strcat('Training_TrialOrder_Full', ...
    '.csv'),'Delimiter', ',');

