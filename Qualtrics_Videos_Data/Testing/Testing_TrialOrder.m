% This script is to organise trial list for qualtrics
% By Jingwen Chai
% V1, 16 Oct 21

%% Initialise workspace
clear all; clc;
cd('/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/VMAC_Videos/VMAC_MP4/Testing');

%% Import csv that contains target & distractor identities
filename = '/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/VMAC_Videos/VMAC_MP4/Testing/2021_11_01_1101_Testing_updated.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%s%s%s%s%s%s%s%s%s%s%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, ...
    'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = dataArray{col};
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));

for col=[2,4,6,7,8,9,10]
    % Converts strings in the input cell array to numbers. Replaced non-numeric
    % strings with NaN.
    rawData = dataArray{col};
    for row=1:size(rawData, 1);
        % Create a regular expression to detect and remove non-numeric prefixes and
        % suffixes.
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData{row}, regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if any(numbers==',');
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(thousandsRegExp, ',', 'once'));
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric strings to numbers.
            if ~invalidThousandsSeparator;
                numbers = textscan(strrep(numbers, ',', ''), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch me
        end
    end
end

rawNumericColumns = raw(:, [2,4,6,7,8,9,10]);
rawCellColumns = raw(:, [1,3,5]);

updated1 = table;
updated1.trialtype = rawCellColumns(:, 1);
updated1.trialnumber = cell2mat(rawNumericColumns(:, 1));
updated1.T1 = rawCellColumns(:, 2);
updated1.D1 = cell2mat(rawNumericColumns(:, 2));
updated1.T2 = rawCellColumns(:, 3);
updated1.D2 = cell2mat(rawNumericColumns(:, 3));
updated1.RSVPlength = cell2mat(rawNumericColumns(:, 4));
updated1.fixationduration = cell2mat(rawNumericColumns(:, 5));
updated1.RSVPduration = cell2mat(rawNumericColumns(:, 6));
updated1.videoduration = cell2mat(rawNumericColumns(:, 7));

TrList = updated1;

clearvars updated1 filename delimiter startRow formatSpec fileID ...
    dataArray ans raw col numericData rawData row regexstr result ... 
    numbers invalidThousandsSeparator thousandsRegExp me rawNumericColumns ...
    rawCellColumns;

%% Import Trial Order csv
filename = '/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/VMAC_Videos/VMAC_MP4/Testing/testing_trial_order_updated_2.csv';
delimiter = ',';
startRow = 2;
formatSpec = '%s%s%s%s%s%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, ...
    'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = dataArray{col};
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));

for col=[1,2,5]
    % Converts strings in the input cell array to numbers. Replaced non-numeric
    % strings with NaN.
    rawData = dataArray{col};
    for row=1:size(rawData, 1);
        % Create a regular expression to detect and remove non-numeric prefixes and
        % suffixes.
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData{row}, regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if any(numbers==',');
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(thousandsRegExp, ',', 'once'));
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric strings to numbers.
            if ~invalidThousandsSeparator;
                numbers = textscan(strrep(numbers, ',', ''), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch me
        end
    end
end

rawNumericColumns = raw(:, [1,2,5]);
rawCellColumns = raw(:, [3,4]);

TrOrder = table;
TrOrder.trial_number = cell2mat(rawNumericColumns(:, 1));
TrOrder.block = cell2mat(rawNumericColumns(:, 2));
TrOrder.trial_type = rawCellColumns(:, 1);
TrOrder.SpTr = rawCellColumns(:, 2);
TrOrder.SpNum = cell2mat(rawNumericColumns(:, 3));

clearvars filename delimiter startRow formatSpec fileID dataArray ...
    ans raw col numericData rawData row regexstr result numbers ... 
    invalidThousandsSeparator thousandsRegExp me rawNumericColumns ...
    rawCellColumns;

%% Add target, distractor identities & other information to Trial Order

% Sort TrOrder by SpTr & SpNum
TrOrder = sortrows(TrOrder, {'SpTr', 'SpNum'});

% Sort TrList by trialtrype & trialnumber
TrList = sortrows(TrList, {'trialtype', 'trialnumber'});

% Add columns from TrList to TrOrder
TrOrder.T1 = TrList.T1;
TrOrder.D1 = TrList.D1;
TrOrder.T2 = TrList.T2;
TrOrder.D2 = TrList.D2;
TrOrder.RSVPlength = TrList.RSVPlength;
TrOrder.fixationduration = TrList.fixationduration;
TrOrder.RSVPduration = TrList.RSVPduration;
TrOrder.videoduration = TrList.videoduration;

%% !!!!!!!! DO NOT USE EVALUATE CURRENT SECTION FOR THIS SECTION !!!!!!!!!
% Define reward factor based on counterbalancing
% Import spreadsheet with counterbalancing info
CounterB = readtable('RewardCounterbalanceChart_V2.xlsx');
%Duplicate groups for short SOA and long SOA conditions
CounterB.Group(4:6) = (1:3)';
% Rename colours to corresponding conditions
CounterB(1,[2:4]) = cell2table({'RD2', 'BD2', 'GD2'});
CounterB(2,[2:4]) = cell2table({'GD2', 'RD2', 'BD2'});
CounterB(3,[2:4]) = cell2table({'BD2', 'GD2', 'RD2'});
CounterB(4,[2:4]) = cell2table({'RD8', 'BD8', 'GD8'});
CounterB(5,[2:4]) = cell2table({'GD8', 'RD8', 'BD8'});
CounterB(6,[2:4]) = cell2table({'BD8', 'GD8', 'RD8'});
% Sort table 
CounterB = sortrows(CounterB, 'Group');

% !!!! Do not proceed until Group number is updated !!!!

%Identify current group we want to use to update TrOrder
Group = 1; % Change to group number we want 1,2or3

CounterB_Current = CounterB(CounterB.Group == Group,:);
RewName = {'HR', 'LR', 'NR'};

% Create variable with empty cells to assign strings
TrOrder.RewardD = cell(120,1);

% Add in reward labels 
for row = 1:height(CounterB_Current)
    for col = 2:4
        for j = 1:height(TrOrder)
            if strcmp(char(TrOrder.SpTr(j)), ... 
                    char(table2cell(CounterB_Current(row,col)))) == 1
                TrOrder.RewardD(j) = RewName(col-1);
            end    
        end
    end    
end
%%

% Add in column with name for qualtrics
% Create variable with empty cells to assign strings
TrOrder.LinkName = cell(120,1);

% Assign names
for row2 = 1:height(TrOrder)
    if strcmp(char(TrOrder.SpTr(row2)),'BD2') == 1 || ... 
            strcmp(char(TrOrder.SpTr(row2)),'RD2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)),'GD2') == 1 
        TrOrder.LinkName(row2) = {strcat('Lag2/', ...
            char(TrOrder.trial_type(row2)))}; 
    elseif strcmp(char(TrOrder.SpTr(row2)),'BD8') == 1 || ... 
            strcmp(char(TrOrder.SpTr(row2)),'RD8') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)),'GD8') == 1 
        TrOrder.LinkName(row2) = {strcat('Lag8/', ...
            char(TrOrder.trial_type(row2)))}; 
    else
        TrOrder.LinkName(row2) = TrOrder.LinkName(row2);
    end
end

% Create variable with empty cells to assign strings
TrOrder.Colour = cell(120,1);

% Assign names for T2 colour conditions
for row2 = 1:height(TrOrder)
    if strcmp(char(TrOrder.SpTr(row2)), 'BD2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)), 'BD8') == 1 
        TrOrder.Colour(row2) = {'blue'};
    elseif strcmp(char(TrOrder.SpTr(row2)), 'RD2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)), 'RD8') == 1 
        TrOrder.Colour(row2) = {'red'};
    elseif strcmp(char(TrOrder.SpTr(row2)), 'GD2') == 1 || ...
            strcmp(char(TrOrder.SpTr(row2)), 'GD8') == 1 
        TrOrder.Colour(row2) = {'green'};
    else
        TrOrder.Colour(row2) = TrOrder.Colour(row2);
    end
end

%Sort TrOrder by trial_number
TrOrder = sortrows(TrOrder, 'trial_number');

%%

% Write csv
writetable(TrOrder, strcat('Testing_TrialOrder_Full_Group', ...
    num2str(Group),'.csv'),'Delimiter', ',');