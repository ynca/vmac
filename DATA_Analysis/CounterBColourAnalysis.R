## This is a script for analysing AB effects based on colour to check if colour is a systematic bias
## Version 1, 14 Nov 21

rm(list=ls())
setwd("/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/DATA_Analysis")

############ FOR Training block Analysis (B1 & B2)
## Import CSVs that contain Group 1 and Group 2 data 
df.Grp1 <- read.csv('ProcData_AB_Colour_Grp1.csv') #For Group1 data
df.Grp2 <- read.csv('ProcData_AB_Colour_Grp2.csv') #For Group2 data
df.Grp3 <- read.csv('ProcData_AB_Colour_Grp3.csv') #For Group3 data
# Combine the two data frames together
df.GrpCombined <- rbind(df.Grp1, df.Grp2, df.Grp3)

## Identify participants with technical difficulties for analysis exclusion
ExcludeList <- c(4210,4254,4445,4533,4447,3957,4535)
## Delete rows that contain subs in the exclude list
df.GrpCombined <- df.GrpCombined[ ! df.GrpCombined$SubID %in% ExcludeList, ]

## Exclude subjects with any data that fall out of 2SD below the mean
# Identify value to exclude
ExcludeProp <- mean(df.GrpCombined$PropT2T1_Acc_BCombined) - 
  2*sd(df.GrpCombined$PropT2T1_Acc_BCombined)
# Based on ExcludeProp, identify subjects to exclude data
ExcludeList.SD <- df.GrpCombined$SubID[df.GrpCombined$PropT2T1_Acc_BCombined < ExcludeProp]
# Delete rows that contain subs in ExcludeList.SD
df.GrpCombined <- df.GrpCombined[ ! df.GrpCombined$SubID %in% ExcludeList.SD, ]

# Convert SubID to factor
df.GrpCombined$SubID <- as.factor(df.GrpCombined$SubID)

## Analyse Lag*Colour on PropT2T1AccCombined
library(afex)
library(emmeans)

##### rm-ANOVA analysis of accuracy rates for Lag*Colour
fit.aov.1 <- aov_ez("SubID", "PropT2T1_Acc_BCombined", df.GrpCombined, 
                    within = c("Colour", "Lag"))
summary(fit.aov.1)
emmeans(fit.aov.1, "Lag", by ="Colour")



############ END OF  Training block Analysis (B1 & B2)

############ FOR Testing block Analysis (C1)
rm(list=ls())
setwd("/Users/jingwenchai/Documents/PhD_ACTLab_iMac/VMAC_personal/DATA_Analysis")
## Import CSVs that contain Group 1 and Group 2 data 
df.Grp1 <- read.csv('ProcData_AB_Colour_C1_Grp1.csv') #For Group1 data
df.Grp2 <- read.csv('ProcData_AB_Colour_C1_Grp2.csv') #For Group2 data
df.Grp3 <- read.csv('ProcData_AB_Colour_C1_Grp3.csv') #For Group3 data
# Combine the two data frames together
df.GrpCombined <- rbind(df.Grp1, df.Grp2,df.Grp3)

## Identify participants with technical difficulties for analysis exclusion
ExcludeList <- c(4210,4254,4445,4424,4533,4447,3957,4535)
## Delete rows that contain subs in the exclude list
df.GrpCombined <- df.GrpCombined[ ! df.GrpCombined$SubID %in% ExcludeList, ]

df.GrpCombined$SubID <- as.factor(df.GrpCombined$SubID)

## Analyse Lag*Colour on PropT2T1AccCombined
library(afex)
library(emmeans)

##### rm-ANOVA analysis of accuracy rates for Lag*Colour
fit.aov.1 <- aov_ez("SubID", "PropT2T1_Acc", df.GrpCombined, 
                    within = c("Colour", "Lag"))
summary(fit.aov.1)
emmeans(fit.aov.1, "Lag", by ="Colour")
############ FOR Testing block Analysis (C1)
